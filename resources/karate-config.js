function() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    baseUrl: 'https://staging.aiq.io/rest',
    tagstorun: karate.properties['tagstorun']
  }
  if (env == 'production') {
	  config.baseUrl = 'http://api.aiq.io';
  } else if (env == 'staging') {
	  config.baseUrl = 'https://staging.aiq.io/staging/rest';
  }

  karate.log('karate.env =', karate.env);
  karate.log('config.baseUrl =', config.baseUrl);
  karate.log('config.tagstorun =', config.tagstorun);

  return config;
}