package com.mediaiq.aiqx.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * This Enum is used to define and use the config Properties based on profile
 */
public  enum ConfigHelper {

  INSTANCE;
  private Properties props;

  ConfigHelper() {
    try {
      loadProperties(ConfigHelper.class.getClassLoader().getResourceAsStream("config.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  ConfigHelper(InputStream inputStream) throws IOException {
    loadProperties(inputStream);
  }

  private void loadProperties(InputStream inputStream) throws IOException {
    props = new Properties();
    props.load(inputStream);
  }

  /**
   * This method returns the value of a given Key from the Config
   *
   * @return value of the given key from the config
   */
  public String getValueFromConfig(String key) {
    return props.getProperty(key);
  }
}
