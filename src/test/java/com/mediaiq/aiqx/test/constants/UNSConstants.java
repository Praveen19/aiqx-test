package com.mediaiq.aiqx.test.constants;

import java.util.Arrays;
import java.util.List;

public class UNSConstants {

  public static final List<String> GEOS = Arrays.asList("USA", "United Kingdom", "Germany",
      "Canada", "Australia");

  public static final List<String> australiaRegionList = Arrays.asList("New South Wales","Australian Capital "
                                              +"Territory","Victoria","South Australia","Northern Territory",
                                              "Queensland","Tasmania","Western Australia");

  /** Below list we might used in the other scenarios */
  /*
   * public static final List<String> QUERIES = Arrays.asList(
   * "ebay UK - DMP - 6494598 - H&G Users", "3035428", "msn.com", "news", "iPhone", "pc", "Apple",
   * "Age - 18-24");
   * 
   * public static final List<String> ENTITYTYPE = Arrays.asList("Brand", "Audience",
   * "Owned Audience", "Owned", "Keyword", "Site Domain", "Brand v2", "Demographic");
   */

  public static final String downloadFilepath = "DownloadImage";

}
