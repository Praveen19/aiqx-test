package com.mediaiq.aiqx.test.modules;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import com.mediaiq.aiqx.test.constants.UNSConstants;
import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxGeoPerformance extends Base {

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);
  public static String url = "";
  public static final JavascriptExecutor jse = (JavascriptExecutor) driver;

  public AiqxGeoPerformance(WebDriver driver) {
    super(driver);
  }

  /** it validates the geo thumbnail should appear in the report loading screen */
  public static void
      i_validate_that_geo_thumbnail_report_should_appear_in_the_report_loading_screen() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UNSGeo_Thumbnail));
    assertTrue(AIQXUNS_Object.UNSGeo_Thumbnail.isDisplayed());
    driver.navigate().back();
  }

  /**
   * it validates the city/region name present in geo thumbnail with region selected in geo picker
   */
  public static void
      i_validate_the_city_names_present_in_the_geo_thumbnail_report_with_its_user_geo() {

    for (final WebElement city : AIQXUNS_Object.UNSGeo_ThumbnailCity) {
      if (!UNSConstants.australiaRegionList.contains(city.getText())) {
        assertTrue(false);
      }
    }

    driver.navigate().back();
  }
  /** Clicks on geo report thumbnail */
  public static void i_click_on_the_geo_report_thumbnail() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UNSGeo_Thumbnail));
    AIQXUNS_Object.UNSGeo_Thumbnail.click();
  }
  /**
   * it validates geo report title
   *
   * @throws Throwable
   */
  public static void i_validate_the_geo_report_title() throws Throwable {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSGeoReportTitle));
    assertEquals("Geo Performance", AIQXUNS_Object.UNSGeoReportTitle.getText());
    AIQXUNS_Object.DiscoverTab.click();
    Thread.sleep(500);
  }

  /**
   * it navigates to geo detail report screen
   *
   * @param entityname
   * @param entitytype
   * @throws Throwable
   */
  public static void i_should_be_on_the_geo_detail_report_screen_for_the_searched_of_its_type_as(
      String entityname, String entitytype) throws Throwable {
    AiqxKeyword.i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
    AiqxKeyword
        .i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
            entitytype);
    i_click_on_the_geo_report_thumbnail();
  }

  /**
   * it copies the share link of geo trend report
   *
   * @throws Throwable
   */
  public static void i_copied_the_share_link_of_geo_trend_report() throws Throwable {
    Thread.sleep(5000);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.ShareURLBox));
    url =
        (String)
            jse.executeScript("return document.getElementsByClassName(\"download-url\")[0].value");
    Log.info(url);
  }

  /** it validates geo report should display for the share link */
  public static void geo_report_should_display_for_the_share_link() {
    AIQXUNS_Object.DiscoverTab.click();
  }

  /** Checks the copied share link of geo report is working fine */
  public static void i_browsed_the_copied_share_link_of_geo_report_in_any_browser_tab() {
    jse.executeScript("window.open();");
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.get(url);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSGeoReportTitle));
    assertTrue(AIQXUNS_Object.UNSGeoReportTitle.isDisplayed());
    assertTrue(!isElementPresent(AIQXUNS_Object.ExportReportButton));
    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }
}
