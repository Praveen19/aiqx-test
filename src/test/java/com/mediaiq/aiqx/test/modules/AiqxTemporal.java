package com.mediaiq.aiqx.test.modules;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.common.collect.Sets;
import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxTemporal extends Base {

  public AiqxTemporal(WebDriver driver) {
    super(driver);
    // TODO Auto-generated constructor stub
  }

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);
  public static String url = "";
  public static final JavascriptExecutor jse = (JavascriptExecutor) driver;

  public static void
      i_validate_that_temporal_thumbnail_report_should_appear_in_the_report_loading_screen() {

    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UnsTemporalThumbnailReport));
    assertTrue(AIQXUNS_Object.UnsTemporalThumbnailReport.isDisplayed());
    driver.navigate().back();
  }

  public static void i_click_on_the_temporal_report_thumbnail() {

    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UnsTemporalThumbnailReport));
    AIQXUNS_Object.UnsTemporalThumbnailReport.click();
  }

  public static void i_validate_the_temporal_report_title() throws Throwable {

    // step need to implement
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UnsTemporalReportTitle));
    assertEquals("Temporal Trends", AIQXUNS_Object.UnsTemporalReportTitle.getText());
    AIQXUNS_Object.DiscoverTab.click();
    Thread.sleep(500);
  }

  public static void
      i_should_be_on_the_temporal_trends_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {

    AiqxKeyword.i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
    AiqxKeyword
        .i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
            entitytype);
    i_click_on_the_temporal_report_thumbnail();
  }

  public static void tod_button_should_be_clickable() {

    assertTrue(AIQXUNS_Object.todButton.isEnabled());
  }

  public static void dow_button_should_be_clicakble() {

    assertTrue(AIQXUNS_Object.dowButton.isEnabled());
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void hours_axis_should_have_value_between_zero_to_twentythree() {
    int minHoursValue = 0;
    int maxHoursValue = 23;
    int hoursDefualtValue = 0;

    int hourValue = AIQXUNS_Object.todXaxis.size();

    for (int i = 0; i < hourValue; i++) {
      if (!AIQXUNS_Object.todXaxis.get(i).getText().isEmpty()) {
        hoursDefualtValue = Integer.parseInt(AIQXUNS_Object.todXaxis.get(i).getText());
        assertTrue(minHoursValue <= hoursDefualtValue && hoursDefualtValue <= maxHoursValue);
      }
    }
  }

  public static boolean hour_axis_should_have_no_duplicate_hour_value() {

    List<Integer> hoursvalues = new ArrayList<Integer>();
    int hourValue = AIQXUNS_Object.todXaxis.size();
    for (int i = 0; i < hourValue; i++) {
      if (!AIQXUNS_Object.todXaxis.get(i).getText().isEmpty()) {
        hoursvalues.add(Integer.parseInt(AIQXUNS_Object.todXaxis.get(i).getText()));

        Set<Integer> hours = new HashSet<Integer>();
        for (int num : hoursvalues) {
          if (hours.contains(num)) {
            return false;
          }
          hours.add(num);
        }
      }
    }
    return true;
  }

  public static void i_click_on_discover_breadcrumb_tab_to_go_discover_page() {
    AIQXUNS_Object.DiscoverTab.click();
  }

  /** Commenting this piece of code as index value need to be validated with db */
  /**
   * public static void index_value_in_tod_report_should_lie_between_minus_one_to_plus_one() throws
   * Throwable {
   *
   * <p>double minIndexvalue = -1; double maxIndexvalue = 1; double Indexvalue = 0;
   *
   * <p>int Indexes = AIQXUNS_Object.todYaxis.size();
   *
   * <p>for (int i = 0; i < Indexes; i++) { if (!AIQXUNS_Object.todYaxis.get(i).getText().isEmpty())
   * { Indexvalue = Double.parseDouble(AIQXUNS_Object.todYaxis.get(i).getText());
   * assertTrue(minIndexvalue <= Indexvalue && Indexvalue <= maxIndexvalue); }
   *
   * <p>} AIQXUNS_Object.DiscoverTab.click(); }
   */
  public static void i_click_on_day_of_week_button() {

    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.dowButton));
    AIQXUNS_Object.dowButton.click();
  }

  /** Commenting this piece of code as index value need to be validated with db */
  /**
   * public static void index_value_in_dow_report_should_lie_between_minus_one_to_plus_one() throws
   * Throwable {
   *
   * <p>double minIndexvalue = -1; double maxIndexvalue = 1; double Indexvalue = 0;
   *
   * <p>int Indexes = AIQXUNS_Object.todYaxis.size();
   *
   * <p>for (int i = 0; i < Indexes; i++) { if (!AIQXUNS_Object.todYaxis.get(i).getText().isEmpty())
   * { Indexvalue = Double.parseDouble(AIQXUNS_Object.todYaxis.get(i).getText());
   * assertTrue(minIndexvalue <= Indexvalue && Indexvalue <= maxIndexvalue); }
   *
   * <p>} }
   */
  public static void day_axis_should_be_in_sorted_order() {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.Mon));
    final Set<String> days = Sets.newHashSet("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun");
    try {
      for (int i = 0; i < AIQXUNS_Object.todXaxis.size(); i++) {
        assertTrue(days.contains(AIQXUNS_Object.todXaxis.get(i).getText()));
      }
    } catch (Exception ex) {
      Log.info("Validation failed for TOD Days");
    }
  }

  public static void i_copied_the_share_link_of_temporal_trend_report() throws Throwable {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.exporturlbox));
    url =
        (String)
            jse.executeScript("return document.getElementsByClassName(\"download-url\")[0].value");
    Log.info(url);
  }

  public static void i_browsed_the_copied_share_link_of_temporal_report_in_any_browser_tab() {
    jse.executeScript("window.open();");
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.get(url);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UnsTemporalReportTitle));
    assertTrue(AIQXUNS_Object.UnsTemporalReportTitle.isDisplayed());
    assertTrue(!isElementPresent(AIQXUNS_Object.ExportReportButton));
    assertTrue(AIQXUNS_Object.todButton.isDisplayed());
    assertTrue(AIQXUNS_Object.dowButton.isDisplayed());
    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }

  public static void temporal_report_should_display_for_the_share_link() {
    AIQXUNS_Object.DiscoverTab.click();
  }
}
