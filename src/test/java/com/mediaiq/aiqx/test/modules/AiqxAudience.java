package com.mediaiq.aiqx.test.modules;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxAudience extends Base {

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);
  public static String url = "";
  public static final JavascriptExecutor jse = (JavascriptExecutor) driver;

  public AiqxAudience(WebDriver driver) {
    super(driver);
  }

  /** Selecting the geo from the geo picker */
  public static void i_click_and_set_the_from_geo_picker(String geo) throws Exception {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.GeoButton));
    Thread.sleep(500);
    AIQXUNS_Object.GeoButton.click();

    for (int i = 0; i < AIQXUNS_Object.GeosList.size(); i++) {
      if (AIQXUNS_Object.GeosNames.get(i).getText().equalsIgnoreCase(geo)) {
        AIQXUNS_Object.GeosList.get(i).click();
        break;
      }
    }
  }

  /** Clicks on audience report thumbnail */
  public static void i_click_on_the_audience_report_thumbnail() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UNSAudience_Thumbnail));
    AIQXUNS_Object.UNSAudience_Thumbnail.click();
  }

  /** It ensures audience thumbnail report is rendered in report loading screen */
  public static void
      i_validate_that_audience_thumbnail_report_should_appear_in_the_report_loading_screen() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UNSAudience_Thumbnail));
    assertTrue(AIQXUNS_Object.UNSAudience_Thumbnail.isDisplayed());
    driver.navigate().back();
  }

  /** It validates the audience report title */
  public static void i_validate_the_audience_report_title() throws Exception {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSAudienceReportTitle));
    assertEquals("Audience Explorer", AIQXUNS_Object.UNSAudienceReportTitle.getText());
    AIQXUNS_Object.DiscoverTab.click();
    Thread.sleep(500);
  }

  /**
   * @param entityname entityname
   * @param entitytype entitytype It navigates automation code to audience trends detail report
   *     screen
   */
  public static void
      i_should_be_on_the_audience_trends_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    AiqxKeyword.i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
    AiqxKeyword
        .i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
            entitytype);
    i_click_on_the_audience_report_thumbnail();
  }

  /** It ensures every segment to be unique in audience report */
  public static void segment_name_should_be_unique() {

    final int segmentIndexes = AIQXUNS_Object.AudienceSegmentNames.size();
    Map<String, Integer> SegmentNames = new HashMap<String, Integer>();
    try {
      for (int i = 0; i < segmentIndexes; i++) {
        if (SegmentNames.containsKey(AIQXUNS_Object.AudienceSegmentNames.get(i).getText())) {
          assertTrue(false);
        } else {
          SegmentNames.put(AIQXUNS_Object.AudienceSegmentNames.get(i).getText(), 1);
        }
      }
    } catch (Exception ex) {
      Log.info("Validation failed for Audience unique segment name ");
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  /** It ensures valid brand names corresponding to every segment row in Audience report */
  public static void brand_name_should_be_there_for_each_segment_name() {

    final int brandIndexes = AIQXUNS_Object.UNSAudienceBrandtext.size();
    try {
      for (int i = 0; i < brandIndexes / 2; i++) {
        List<WebElement> Img =
            AIQXUNS_Object.UNSAudienceBrandtext.get(i).findElements(By.xpath("./img"));
        boolean CheckImg =
            ((Img.size() >= 1)
                && (Img.get(0).getAttribute("src") != null)
                && (!Img.get(0).getAttribute("src").isEmpty()));
        if (CheckImg) {
          assertTrue(CheckImg);
        } else {
          List<WebElement> Txt =
              AIQXUNS_Object.UNSAudienceBrandtext.get(i)
                  .findElements(By.className("usn-provider-title"));
          boolean checkText =
              ((Txt.size() >= 1)
                  && (Txt.get(0).getText() != null)
                  && (!Txt.get(0).getText().isEmpty()));
          assertTrue(checkText);
        }
      }
    } catch (Exception ex) {
      Log.info("Validation failed for brand name");
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  /** It ensure all the segments present in the audience report have index define between -1 to 1 */
  public static void index_value_in_audience_report_should_lie_between_minus_one_to_plus_one() {
    double minperfIndexvalue = 0;
    double perfvalue ;

    final int perfIndexes = AIQXUNS_Object.PerformanceIndexvalue.size();
    for (int i = 0; i < perfIndexes; i++) {
      if (!AIQXUNS_Object.PerformanceIndexvalue.get(i).getText().isEmpty()) {
        perfvalue = Double.parseDouble(AIQXUNS_Object.PerformanceIndexvalue.get(i).getText());
        assertTrue(minperfIndexvalue <= perfvalue);
      }
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  /** It ensure that it copied the share link of audience report */
  public static void i_copied_the_share_link_of_audience_trend_report() {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.exporturlbox));
    url =
        (String)
            jse.executeScript("return document.getElementsByClassName(\"download-url\")[0].value");
    Log.info(url);
  }

  /**
   * it ensure that copied share link will browsed in new tab or new window browser to check the
   * report
   */
  public static void i_browsed_the_copied_share_link_of_audience_report_in_any_browser_tab() {
    jse.executeScript("window.open();");
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.get(url);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSAudienceReportTitle));
    assertTrue(AIQXUNS_Object.UNSAudienceReportTitle.isDisplayed());
    assertTrue(!isElementPresent(AIQXUNS_Object.ExportReportButton));
    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }

  public static void audience_report_should_display_for_the_share_link() {
    AIQXUNS_Object.DiscoverTab.click();
  }
}
