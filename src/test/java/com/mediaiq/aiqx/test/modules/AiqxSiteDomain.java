package com.mediaiq.aiqx.test.modules;

import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/** */
public class AiqxSiteDomain extends Base {

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);
  public static String url = "";
  public static final JavascriptExecutor jse = (JavascriptExecutor) driver;
  public static String selectedSiteDomainCatgeory = "";

  public AiqxSiteDomain(WebDriver driver) {
    super(driver);
  }

  /* To validate site domain thumbnail is available for different entity types */
  public static void i_click_on_the_site_domain_report_thumbnail() {
    wait.until(
        ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UnsSiteDomainThumbnailReport));
    AIQXUNS_Object.UnsSiteDomainThumbnailReport.click();
  }

  /* To validate report title */
  public static void i_validate_the_site_domain_report_title() throws Exception {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSSiteDomainReportTitle));
    assertEquals("Top Site Domains", AIQXUNS_Object.UNSSiteDomainReportTitle.getText());
    AIQXUNS_Object.DiscoverTab.click();
    Thread.sleep(500);
  }

  public static void
      i_validate_that_site_domain_thumbnail_report_should_appear_in_the_report_loading_screen() {
    wait.until(
        ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UnsSiteDomainThumbnailReport));
    assertTrue(AIQXUNS_Object.UnsSiteDomainThumbnailReport.isDisplayed());
    driver.navigate().back();
  }

  public static void
      i_should_be_on_the_site_domain_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    AiqxKeyword.i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
    AiqxKeyword
        .i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
            entitytype);
    i_click_on_the_site_domain_report_thumbnail();
  }

  public static void no_duplicate_domains_name_should_present() {

    final int siteDomainIndexes = AIQXUNS_Object.SiteDomainsNames.size();
    Map<String, Integer> SiteDomainNames = new HashMap<String, Integer>();
    try {
      for (int i = 0; i < siteDomainIndexes; i++) {
        if (AIQXUNS_Object.SiteDomainsNames.get(i).getText().isEmpty()) {
          continue;
        }
        if (SiteDomainNames.containsKey(AIQXUNS_Object.SiteDomainsNames.get(i).getText())) {
          assertTrue(false);
        } else {
          SiteDomainNames.put(AIQXUNS_Object.SiteDomainsNames.get(i).getText(), 1);
        }
      }
    } catch (Exception ex) {
      Log.info("Validation failed for Site Domain unique domain name. Exception: " + ex);
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void site_domain_category_name_should_not_be_empty() {
    final int count = AIQXUNS_Object.UNSSiteDomainReportCategoryName.size() / 2;
    try {
      for (int i = 0; i < count; i++) {
        assertTrue(!AIQXUNS_Object.UNSSiteDomainReportCategoryName.get(i).getText().isEmpty());
      }
    } catch (Exception ex) {
      Log.info(
          "Validation failed for Site Domain Category name should not be empty. Exception: " + ex);
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void all_the_filter_checkbox_option_should_display_in_detail_screen() {
    AIQXUNS_Object.UNSSiteDomainReportVolumeFilter.isDisplayed();
  }

  public static void all_category_button_should_be_enable() {
    AIQXUNS_Object.UNSSiteDomainReportAllCategoriesButton.get(0).isEnabled();
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void i_click_on_low_bucket_filter_on_detail_screen() {
    AIQXUNS_Object.UNSSiteDomainReportVolumeLowCheckbox.get(0).click();
  }

  public static void all_domains_should_get_filter_with_volume_as_low() {
    final int count = AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsLow.size();
    try {
      for (int i = 0; i < count; i++) {
        if (AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsLow.get(i)
                .getText()
                .equals(AIQXUNS_Object.UNSSiteDomainReportVolumeFilterHigh.getText())
            || AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsLow.get(i)
                .getText()
                .equals(AIQXUNS_Object.UNSSiteDomainReportVolumeFilterMedium.getText())) {
          assertTrue(false);
        }
      }
    } catch (Exception e) {
      Log.info("Validation failed for Site Domain Volume Filter as low. Exception: " + e);
    }
  }

  public static void i_click_on_medium_bucket_filter_on_detail_screen() {
    AIQXUNS_Object.UNSSiteDomainReportVolumeMediumCheckbox.get(0).click();
  }

  public static void all_domain_should_get_filter_with_volume_as_medium() {
    final int count = AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsMedium.size();
    try {
      for (int i = 0; i < count; i++) {
        if (AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsMedium.get(i)
                .getText()
                .equals(AIQXUNS_Object.UNSSiteDomainReportVolumeFilterHigh.getText())
            || AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsMedium.get(i)
                .getText()
                .equals(AIQXUNS_Object.UNSSiteDomainReportVolumeFilterLow.getText())) {
          assertTrue(false);
        }
      }
    } catch (Exception e) {
      Log.info("Validation failed for Site Domain Volume Filter as medium. Exception: " + e);
    }
  }

  public static void i_click_on_high_bucket_filter_with_volume_as_high() {
    AIQXUNS_Object.UNSSiteDomainReportVolumeHighCheckbox.get(0).click();
  }

  public static void all_domain_should_filter_with_volume_as_high() {
    final int count = AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsHigh.size();
    try {
      for (int i = 0; i < count; i++) {
        if (AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsHigh.get(i)
                .getText()
                .equals(AIQXUNS_Object.UNSSiteDomainReportVolumeFilterLow.getText())
            || AIQXUNS_Object.UNSSiteDomainReportVolumeValueAsHigh.get(i)
                .getText()
                .equals(AIQXUNS_Object.UNSSiteDomainReportVolumeFilterMedium.getText())) {
          assertTrue(false);
        }
      }
    } catch (Exception e) {
      Log.info("Validation failed for Site Domain Volume Filter as high. Exception: " + e);
    }
  }

  public static void i_click_on_all_categories_button() {
    AIQXUNS_Object.UNSSiteDomainReportAllCategoriesButton.get(0).click();
  }

  public static void i_select_any_category_from_the_all_categories_drop_down_box() {
    // selecting a category from the list(for this test case, any category
    // would do so used index 3)
    selectedSiteDomainCatgeory = AIQXUNS_Object.SiteDomainAllCategoriesList.get(3).getText();
    AIQXUNS_Object.SiteDomainAllCategoriesList.get(3).click();
  }

  public static void all_the_domains_should_get_filter_with_the_selected_category() {
    final int count = AIQXUNS_Object.SiteDomainsWithCategorySelected.size() / 2;
    for (int i = 0; i < count; i++) {

      assertTrue(
          selectedSiteDomainCatgeory.equals(
                  AIQXUNS_Object.SiteDomainsWithCategorySelected.get(i).getText())
              || AIQXUNS_Object.SiteDomainsWithCategorySelected.get(i).getText().isEmpty());
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void i_copied_the_share_link_of_site_domain_report() throws Exception {

    Thread.sleep(4000);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.ShareURLBox));
    url =
        (String)
            jse.executeScript("return document.getElementsByClassName(\"download-url\")[0].value");
    Log.info(url);
  }

  public static void i_browsed_the_copied_share_link_of_site_domain_report_in_any_browser_tab() {

    jse.executeScript("window.open();");
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.get(url);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSSiteDomainReportTitle));
    assertTrue(AIQXUNS_Object.UNSSiteDomainReportTitle.isDisplayed());
    assertTrue(!isElementPresent(AIQXUNS_Object.ExportReportButton));
    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }

  public static void site_domain_report_should_display_for_the_share_link() {
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static boolean isClickable(WebElement webe) {
    try {
      WebDriverWait wait = new WebDriverWait(driver, 5);
      wait.until(ExpectedConditions.elementToBeClickable(webe));
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public static void all_the_domains_should_be_clickable() {
    List<WebElement> siteDomainLinks = AIQXUNS_Object.SiteDomainLinks;
    for (int i = 0; i < siteDomainLinks.size(); i++) {
      WebElement aTag = siteDomainLinks.get(i).findElement(By.tagName("a"));
      assertTrue(aTag.getText().length() > 0);
    }
  }

  public static void i_click_on_any_site_domain_link() {
    List<WebElement> siteDomainLinks = AIQXUNS_Object.SiteDomainLinks;
    List<WebElement> aTags = siteDomainLinks.get(0).findElements(By.tagName("a"));
    aTags.get(0).click();
  }

  public static void link_should_be_open_in_new_tab() {
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    assertEquals(windowHandles.size(), 2);
  }

  public static void i_close_the_newly_opened_tab() {
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }

  public static void i_click_on_discover_breadcrum_tab() {
    //		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,0)");
    AIQXUNS_Object.DiscoverTab.click();
  }
}
