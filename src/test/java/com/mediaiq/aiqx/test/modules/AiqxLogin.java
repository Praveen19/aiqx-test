package com.mediaiq.aiqx.test.modules;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxLogin extends Base {

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  static WebDriverWait wait = new WebDriverWait(driver, 10);

  public AiqxLogin(WebDriver driver) {
    // TODO Auto-generated constructor stub

    super(driver);
  }

  public static void ExecuteOne(WebDriver driver) {

    // login code via Valid Username & Password
    Log.info("attempting to login");

    loginPage.userName.sendKeys("devuser");
    Log.info("Enter user name");

    loginPage.password.sendKeys("aiqxCIA2016");
    Log.info("Enter password");

    loginPage.signInButton.click();
    Log.info("Click action is performed on Sign in button");
    Reporter.log("SignIn Action is successfully performed");
  }

  // login code via Google
  public static void ExecuteTwo(WebDriver driver) {

    Log.info("attempting to login via Google");
    loginPage.MIQUserLogin.click();
    Log.info("Login via Google get click");

    loginPage.googleemail.sendKeys("praveen@mediaiqdigital.com");
    Log.info("Enter user name");

    loginPage.NextButton.click();
    Log.info("Next button gets click");

    loginPage.googlepasssword.sendKeys("Praveen1605@");
    Log.info("Enter password");

    loginPage.NextButton.click();
    Log.info("Next button gets click");
    Reporter.log("SignIn Action is successfully performed");
  }

  public static void i_navigate_to_the_aiqx_URL() {

    /**
     * staging env url
     */
     driver.navigate().to("https://staging.aiq.io");
     analyzeLog();


    /** production env url driver.navigate().to("https://aiq.io"); analyzeLog(); */

    /**
     * On the Fly product url driver.navigate().to("http://localhost");
     *
     * Log.info(driver.getCurrentUrl());
     * analyzeLog();
     * driver.manage().timeouts().implicitlyWait(20,
     * TimeUnit.SECONDS);
     */
  }

  public static void i_enter_invalid_and_in_the_login_page_and_click_on_sign_in_button(
      String username, String password) {
    Log.info("Attempting to Login in With Invalid Credential");
    loginPage.userName.sendKeys(username);
    Log.info("Enter user name");

    loginPage.password.sendKeys(password);
    Log.info("Enter password");

    loginPage.signInButton.click();
    Log.info("Click action is performed on Sign in button");
  }

  public static void i_check_the_validation_message_in_the_logiin_page() {
    assertEquals(AiqxLogin.loginPage.loginerrormesssage.getText(), "Invalid username or password!");
  }

  public static void i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
      String username, String password) {
    // login code via Valid Username & Password
    Log.info("attempting to login");

    loginPage.userName.sendKeys(username);
    Log.info("Enter user name");

    loginPage.password.sendKeys(password);
    Log.info("Enter password");

    loginPage.signInButton.click();
    Log.info("Click action is performed on Sign in button");
    Reporter.log("SignIn Action is successfully performed");
  }

  public static void enter_the_and_and_click_on_sign_in_button(String username, String password) {

    Log.info("attempting to login via Google");
    loginPage.MIQUserLogin.click();
    Log.info("Login via Google get click");

    loginPage.googleemail.sendKeys(username);
    Log.info("Enter user name");

    loginPage.NextButton.click();
    Log.info("Next button gets click");

    loginPage.googlepasssword.sendKeys(password);
    Log.info("Enter password");
    loginPage.googlepasssword.sendKeys(Keys.ENTER);

    Reporter.log("SignIn Action is successfully performed");
  }

  public static void i_verify_the_aiqx_landing_discover_title_and_logged_username_as(
      String username) {
    wait.until(ExpectedConditions.visibilityOf(loginPage.AIQXLandingPage_DiscoverTitle));
    String ExpectedAIQXLandingPageTitle = "DISCOVER";
    assertEquals(ExpectedAIQXLandingPageTitle, loginPage.AIQXLandingPage_DiscoverTitle.getText());
    String s = username.substring(0, 7);
    assertEquals(loginPage.LoggedUserName.getText(), s);
    Log.info(loginPage.LoggedUserName.getText());
  }

  public static void aiqx_landing_discover_title_and_logged_username_as(String username) {
    String ExpectedAIQXLandingPageTitle = "DISCOVER";
    assertEquals(ExpectedAIQXLandingPageTitle, loginPage.AIQXLandingPage_DiscoverTitle.getText());
    Log.info(loginPage.AIQXLandingPage_DiscoverTitle.getText());
    String s = username.substring(0, 7);
    assertEquals(loginPage.LoggedUserName.getText(), s);
    Log.info(loginPage.LoggedUserName.getText());
  }

  public static void click_on_Logout() {
    loginPage.UserLogoutOption.click();
    wait.until(ExpectedConditions.visibilityOf(loginPage.AIQXLogo));
  }
}
