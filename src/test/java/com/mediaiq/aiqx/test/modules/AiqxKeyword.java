package com.mediaiq.aiqx.test.modules;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import com.mediaiq.aiqx.helpers.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.mediaiq.aiqx.test.constants.UNSConstants;
import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxKeyword extends Base {

  public static LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);
  public static String url = "";
  public static String searchQueryName = "";
  public static final JavascriptExecutor jse = (JavascriptExecutor) driver;

  public AiqxKeyword(WebDriver driver) {
    super(driver);
    // TODO Auto-generated constructor stub
  }

  public static void i_triger_the_below_test_step_from_inititate_function_with_the_given_and(
      String entityname, String entitytype) throws Throwable {
    for (String geo : UNSConstants.GEOS) {
      i_click_and_set_the_geo_as(geo);
      i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
      i_verify_the_entity_type_as_in_category_tab(entitytype);
      i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
          entitytype);
      i_validate_that_keyword_thumbnail_report_should_appear_in_the_report_loading_screen();
    }
  }

  public static void i_click_and_set_the_geo_as(String geo) throws Throwable {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.GeoButton));
    Thread.sleep(500);
    AIQXUNS_Object.GeoButton.click();

    for (int i = 0; i < AIQXUNS_Object.GeosList.size(); i++) {
      if (AIQXUNS_Object.GeosNames.get(i).getText().equalsIgnoreCase(geo)) {
        AIQXUNS_Object.GeosList.get(i).click();
        break;
      }
    }
  }

  public static void i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(
      String entityname) throws Throwable {
    AIQXUNS_Object.SearchBox.sendKeys(entityname);
    Thread.sleep(1000);
    // driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
    staleElement(AIQXUNS_Object.EntityTabs);
  }

  public static void i_verify_the_entity_type_as_in_category_tab(String entitytype) {
    boolean resultsFound = false;
    for (WebElement CategoryTab : AIQXUNS_Object.EntityTabs) {
      if (entitytype.toUpperCase().contains(CategoryTab.getText().toUpperCase())) {
        resultsFound = true;
        CategoryTab.click();
        break;
      }
    }
    if (resultsFound == false) {
      throw new RuntimeException("Respective EntityTab doesn't found");
    }
  }

  public static void
      i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
          String entitytype) throws Throwable {
    i_verify_the_entity_type_as_in_category_tab(entitytype);

    Thread.sleep(1000);
    AIQXUNS_Object.SingleEntityResult.click();
  }

  public static void
      i_validate_that_keyword_thumbnail_report_should_appear_in_the_report_loading_screen() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UNSKeyword_Thumbnail));
    assertTrue(AIQXUNS_Object.UNSKeyword_Thumbnail.isDisplayed());
    driver.navigate().back();
  }

  public static void
      i_enter_the_valid_entity_name_as_of_its_entity_type_as_in_search_box_of_landing_screen(
          String entityname, String entitytype) throws Throwable {
    i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
    i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
        entitytype);
  }

  public static void i_click_on_the_keyword_report_thumnail() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UNSKeyword_Thumbnail));
    AIQXUNS_Object.UNSKeyword_Thumbnail.click();
  }

  public static void i_validate_the_keyword_report_title() throws Throwable {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSKeywordReportTitle));
    assertEquals("Keyword Explorer", AIQXUNS_Object.UNSKeywordReportTitle.getText());
    AIQXUNS_Object.DiscoverTab.click();
    Thread.sleep(500);
  }

  public static void
      i_should_be_on_the_keyword_treemap_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
    i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
        entitytype);
    i_click_on_the_keyword_report_thumnail();
  }

  public static void all_categories_button_should_be_clickable() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.AllCategoriesButton));
    assertTrue(AIQXUNS_Object.AllCategoriesButton.isEnabled());
    AIQXUNS_Object.AllCategoriesButton.click();
  }

  public static void all_categories_should_not_contains_blacklist_as_category() {
    String BL = "BlackList";
    try {
      ContainString(BL, AIQXUNS_Object.AllCategoriesList);
    } catch (Exception e) {
      Log.info("BlackList category is Present in Keyword Report");
    }
  }

  public static void view_button_tabs_should_be_clickable() {
    assertTrue(AIQXUNS_Object.ViewButton(1).isEnabled()); // treemap
    // view
    // tab
    assertTrue(AIQXUNS_Object.ViewButton(2).isEnabled()); // tabular
    // view
    // tab
  }

  public static void i_click_on_tabular_view_button() {
    AIQXUNS_Object.ViewButton(2).click();
  }

  public static void tabular_chart_should_display() {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.KeywordPerfIndexHeader));
    assertTrue(AIQXUNS_Object.KeywordPerfIndexHeader.isDisplayed());
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void
      i_should_be_on_the_keyword_tabular_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    i_should_be_on_the_keyword_treemap_detail_report_screen_for_the_searched_of_its_type_as(
        entityname, entitytype);
    i_click_on_tabular_view_button();
  }

  public static void
      performance_index_value_on_the_keyword_tabular_detail_report_screen_should_be_between_minus_one_to_plus_one() {

    double minperfIndexvalue = 0;
    double perfvalue = 0;

    int perfIndexes = AIQXUNS_Object.PerformanceIndexvalue.size();

    for (int i = 0; i < perfIndexes; i++) {
      if (!AIQXUNS_Object.PerformanceIndexvalue.get(i).getText().isEmpty()) {
        perfvalue = Double.parseDouble(AIQXUNS_Object.PerformanceIndexvalue.get(i).getText());
        assertTrue(minperfIndexvalue <= perfvalue );
      }
    }
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void i_click_on_any_category_from_the_tabular_chart() {
    AIQXUNS_Object.FirstKeywordCategory.click();
  }

  public static void check_all_the_keywords_sort_with_same_clicked_category() {
    String CategoryName = AIQXUNS_Object.FirstKeywordCategory.getText();
    try {
      ContainString(CategoryName, AIQXUNS_Object.KeywordCategory);
    } catch (Exception e) {
      Log.info("Sorting the keyword with Category is failing");
    }
  }

  public static void all_categories_button_shoud_display_the_same_clicked_category_name() {
    assertEquals(
        AIQXUNS_Object.AllCategoriesButton.getText(),
        AIQXUNS_Object.FirstKeywordCategory.getText());
  }

  public static void i_click_on_the_treemap_button_from_the_keyword_tabular_chart_screen() {
    AIQXUNS_Object.ViewButton(1).click();
  }

  public static void i_click_on_the_all_ategories_button() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.AllCategoriesButton));
    AIQXUNS_Object.AllCategoriesButton.click();
  }

  public static void i_select_all_categories_filter() {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.AllCategoryOption));
    AIQXUNS_Object.AllCategoryOption.click();
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void i_click_on_the_export_report_button() {
    searchQueryName = AIQXUNS_Object.searchQueryName.getText();
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.ExportReportButton));
    AIQXUNS_Object.ExportReportButton.click();
  }

  public static void i_click_on_download_as_image_button() throws Throwable {

    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.DownloadAsImageButton));
    Thread.sleep(5000);
    AIQXUNS_Object.DownloadAsImageButton.click();

    Thread.sleep(8000); /*
                         * Sometime due to network issue report took time to get downloaded
                         */
  }

  public static void report_gets_downloaded(String reportType) {

    Log.info(
        "Path to test: "
            + System.getProperty("user.dir")
            + File.separator
            + UNSConstants.downloadFilepath
            + File.separator);

    assertTrue(
        (Base.isFileDownloaded_Ext(
            searchQueryName,
            reportType,
            System.getProperty("user.dir")
                + File.separator
                + UNSConstants.downloadFilepath
                + File.separator,
            ".png")));
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void i_copied_the_share_link() throws Throwable {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.exporturlbox));
    url =
        (String)
            jse.executeScript(
                "return document.getElementsByClassName" + "(\"download-url\")[0].value");
  }

  public static void i_browse_the_copied_share_link_in_any_browser_tab() {
    jse.executeScript("window.open();");
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.get(url);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UNSKeywordReportTitle));
    assertTrue(AIQXUNS_Object.UNSKeywordReportTitle.isDisplayed());
    assertTrue(!isElementPresent(AIQXUNS_Object.ExportReportButton));
    assertTrue(AIQXUNS_Object.ViewButton(1).isEnabled());
    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }

  public static void keyword_report_should_display_for_the_share_link() {
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void likedislike_button_should_be_clickable() throws Throwable {
    assertTrue(AIQXUNS_Object.LikeDislikeButton(1).isDisplayed());
    assertTrue(AIQXUNS_Object.LikeDislikeButton(2).isDisplayed());
    AIQXUNS_Object.DiscoverTab.click();
    Thread.sleep(2000);
  }

  public static void
      i_enter_the_valid_keyword_as_of_its_entity_type_as_in_search_box_of_landing_screen(
          String entityname, String entitytype) throws Throwable {
    i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
  }

  public static void
      duplicate_keywords_should_not_display_in_the_autocomplete_for_the_searched_entityname_as_in_its_respective_tab_of_its_type_as(
          String entityname, String entitytype) throws Throwable {

    i_verify_the_entity_type_as_in_category_tab(entitytype);
    Thread.sleep(1000);
    List<String> list1 = new ArrayList<String>();
    List<String> list2 = new ArrayList<String>();
    ArrayList<String> Final = new ArrayList<String>();

    for (WebElement wb : AIQXUNS_Object.CompleteMatchingAutoCompleteList) {
      list1.add(wb.getText());
    }
    for (WebElement wb1 : AIQXUNS_Object.MatchingAutoCompleteEntityTypeList) {
      // Log.info(wb1.getText()); // debug purpose
      list2.add(wb1.getText());
    }

    for (int i = 0; i < list2.size(); i++) {
      Final.add(list1.get(i) + " " + list2.get(i));
    }

    /** Converting ArrayList to HashSet to remove duplicates if any */
    LinkedHashSet<String> listToSet = new LinkedHashSet<String>(Final);

    /** Creating Arraylist without duplicate values */
    List<String> listWithoutDuplicates = new ArrayList<String>(listToSet);

    assertTrue(listWithoutDuplicates.size() == Final.size());

    /** Clearing the searchbox */
    AIQXUNS_Object.SearchBox.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
    Thread.sleep(1000);
  }
}
