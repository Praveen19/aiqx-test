package com.mediaiq.aiqx.test.modules;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import com.mediaiq.aiqx.helpers.Log;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxUns extends Base {

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);

  static WebDriverWait wait = new WebDriverWait(driver, 30);
  public static JavascriptExecutor jse = (JavascriptExecutor) driver;

  public AiqxUns(WebDriver driver) {
    super(driver);
  }

  public static void i_verify_that_search_box_is_auto_focus() {
    wait.until(ExpectedConditions.visibilityOf(loginPage.SearchBox));
    assertTrue((driver.switchTo().activeElement().equals(loginPage.SearchBox)));
  }

  public static void i_check_the_menu_icon_bar_is_clickable() {
    loginPage.AIQMenuBar.click();
    wait.until(ExpectedConditions.visibilityOf(loginPage.AIQXDiscoverOption));
  }

  public static void i_verify_the_menu_list_items() throws Exception {
    assertTrue(loginPage.AIQXDiscoverOption.getText().equals("DISCOVER"));
    assertTrue(loginPage.AIQXActivateOption.getText().equals("ACTIVATE"));
    assertTrue(loginPage.AIQXConnectOption.getText().equals("CONNECT"));
    Thread.sleep(1000);

    loginPage.AIQXMenuBarCloseButton.click();
  }

  public static void i_verify_the_discover_text_should_present_in_the_landing_screen() {
    assertTrue(loginPage.DiscoverTitle.isDisplayed());
  }

  // Autosuggest
  public static void i_enter_the_substring_as_in_search_box_of_the_laanding_screen(String substring)
      throws Throwable {
    AIQXUNS_Object.SearchBox.sendKeys(substring);
    /*
     * wait.until(ExpectedConditions .visibilityOfAllElements(AIQXUNS_Object.EntityTabs));
     */
    staleElement(AIQXUNS_Object.EntityTabs);
    Thread.sleep(1000);
  }

  public static void
      i_validate_that_the_autosuggest_results_should_contains_the_searched_substring_as(
          String substring) {

    if (AIQXUNS_Object.HighlightedResulttext.size() <= 24) {
      for (int i = 0; i < AIQXUNS_Object.HighlightedResulttext.size(); i++) {
        if (AIQXUNS_Object.HighlightedResulttext.get(i).getText().equalsIgnoreCase(substring)) {
          Log.info("Entered Substring matches with Autosuggest entities successfully");
        }
      }
    }
  }

  public static void i_validate_that_at_max_only_twenty_four_box_should_show_in_the_autosuggest() {
    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    assertTrue(AIQXUNS_Object.HighlightedResulttext.size() <= 24);
    Log.info("AutoComplete ResultsCount: " + AIQXUNS_Object.HighlightedResulttext.size());
    AIQXUNS_Object.SearchBox.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
    assertFalse(AIQXUNS_Object.HighlightedResulttext.size() != 0);
  }

  public static void i_enter_the_exact_search_entity_as_in_search_box_of_report_landing_screen(
      String exactstring) throws Throwable {
    i_enter_the_substring_as_in_search_box_of_the_laanding_screen(exactstring);
  }

  public static void
      i_validate_that_the_autosuggest_results_should_contains_the_exact_search_string_as(
          String exactstring) throws Throwable {
    i_validate_that_the_autosuggest_results_should_contains_the_searched_substring_as(exactstring);
    Log.info("AutoComplete ResultsCount: " + AIQXUNS_Object.HighlightedResulttext.size());
    AIQXUNS_Object.SearchBox.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
    assertFalse(AIQXUNS_Object.HighlightedResulttext.size() != 0);
  }

  public static void i_enter_the_any_valid_entity_id_as_in_search_box_of_report_landing_screen(
      String validid) throws Throwable {
    i_enter_the_substring_as_in_search_box_of_the_laanding_screen(validid);
  }

  public static void
      i_validate_that_autosuggest_should_only_display_the_searched_result_name_in_auto_suggestion() {
    assertTrue(AIQXUNS_Object.SingleEntityResult.isDisplayed());
    AIQXUNS_Object.SearchBox.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
    assertFalse(AIQXUNS_Object.HighlightedResulttext.size() != 0);
  }

  public static void i_enter_the_any_valid_string_as_in_search_box_of_landing_screen(
      String validstring) throws Throwable {
    i_enter_the_substring_as_in_search_box_of_the_laanding_screen(validstring);
  }

  public static void
      i_validate_the_number_of_count_of_results_in_autosuggest_screen_should_match_with_the_count_of_results_in_landing_page() {
    assertEquals(
        String.valueOf(AIQXUNS_Object.HighlightedResulttext.size()),
        AIQXUNS_Object.AutoCompleteResultCount.getText());
  }

  public static void
      i_validate_the_counts_of_categories_showing_in_the_landing_screen_should_match_with_the_number_of_categories_tabs() {
    assertEquals(
        String.valueOf(AIQXUNS_Object.AutoCompleteTabs.size() - 1),
        AIQXUNS_Object.AutoCompleteCategoriesCount.getText());
  }

  public static void i_validate_that_all_the_categories_tabs_are_clickable() {
    for (int i = 0; i < AIQXUNS_Object.AutoCompleteTabs.size(); i++) {
      assertTrue(AIQXUNS_Object.AutoCompleteTabs.get(i).isEnabled());
    }
  }

  public static void
      i_validate_that_results_in_all_the_categories_tabs_are_equal_to_the_count_of_results_in_respective_cetagories_tab() {
    for (int i = 0; i < AIQXUNS_Object.AutoCompleteTabs.size() - 1; i++) {
      AIQXUNS_Object.AutoCompleteTabs.get(i).click();
      wait.until(ExpectedConditions.visibilityOfAllElements(AIQXUNS_Object.HighlightedResulttext));
      assertEquals(
          String.valueOf(AIQXUNS_Object.HighlightedResulttext.size()),
          AIQXUNS_Object.AutoCompleteResultCount.getText());
    }
    AIQXUNS_Object.SearchBox.sendKeys(Keys.CONTROL + "a" + Keys.BACK_SPACE);
  }
}
