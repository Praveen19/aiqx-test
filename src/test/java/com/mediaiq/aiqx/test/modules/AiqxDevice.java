package com.mediaiq.aiqx.test.modules;

import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.mediaiq.aiqx.test.Base;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.AiqxUnsPageObject;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;

public class AiqxDevice extends Base {

  public static final LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final AiqxUnsPageObject AIQXUNS_Object =
      PageFactory.initElements(Hooks.driver, AiqxUnsPageObject.class);
  public static String url = "";
  public static Map<String, Integer> positiveIndexDevices = new HashMap<String, Integer>();
  public static final JavascriptExecutor jse = (JavascriptExecutor) driver;

  public AiqxDevice(WebDriver driver) {
    super(driver);
  }

  /** Clicks on Device report thumbnail */
  public static void i_click_on_the_device_ownership_report_thumbnail() throws Exception {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UnsDeviceThumbnailReport));
    AIQXUNS_Object.UnsDeviceThumbnailReport.click();
  }

  /** It ensures device thumbnail report is rendered in report loading screen */
  public static void
      i_validate_that_device_ownership_thumbnail_report_should_appear_in_the_report_loading_screen()
          throws Exception {
    wait.until(ExpectedConditions.elementToBeClickable(AIQXUNS_Object.UnsDeviceThumbnailReport));
    assertTrue(AIQXUNS_Object.UnsDeviceThumbnailReport.isDisplayed());
    driver.navigate().back();
  }

  /** It validates the device report title */
  public static void i_validate_the_device_ownership_report_title() throws Exception {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UnsDeviceReportTitle));
    assertEquals("Device Ownership", AIQXUNS_Object.UnsDeviceReportTitle.getText());
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void i_check_the_name_of_the_device_type_from_the_device_thumbnail_report()
      throws Exception {
    final int deviceNameIndexes = AIQXUNS_Object.ThumbnailDeviceTypeList.size();
    if (deviceNameIndexes <= 0) {
      assertTrue(false);
    } else {
      try {
        for (int i = 0; i < deviceNameIndexes; i++) {
          if (StringUtils.isEmpty(AIQXUNS_Object.ThumbnailDeviceTypeList.get(i).getText())) {
            assertTrue(false);
          } else {
            positiveIndexDevices.put(
                AIQXUNS_Object.ThumbnailDeviceTypeList.get(i).getText().trim().toLowerCase(), 1);
          }
        }
      } catch (Exception ex) {
        Log.info("Validation failed");
      }
    }
  }

  /**
   * @param entityname entityname
   * @param entitytype entitytype It navigates automation code to device ownership report screen
   */
  public static void i_should_be_on_the_device_detail_report_screen_for_the_searched_of_its_type_as(
      String entityname, String entitytype) throws Throwable {
    AiqxKeyword.i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);

    AiqxKeyword
        .i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
            entitytype);
    i_click_on_the_device_ownership_report_thumbnail();
  }

  /** It ensure that it copied the share link of device report */
  public static void i_copied_the_share_link_of_device_ownership_report() throws Exception {
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.exporturlbox));
    url =
        (String)
            jse.executeScript(
                "return document.getElementsByClassName" + "(\"download-url\")[0].value");
    Log.info(url);
  }

  /**
   * it ensure that copied share link will browsed in new tab or new window browser to check the
   * report
   */
  public static void i_browsed_the_copied_share_link_of_device_report_in_any_browser_tab()
      throws Exception {
    jse.executeScript("window.open();");
    ArrayList<String> windowHandles = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(windowHandles.get(1));
    driver.get(url);
    wait.until(ExpectedConditions.visibilityOf(AIQXUNS_Object.UnsDeviceReportTitle));
    Assert.assertTrue(AIQXUNS_Object.UnsDeviceReportTitle.isDisplayed());
    Assert.assertTrue(!isElementPresent(AIQXUNS_Object.ExportReportButton));

    driver.close();
    driver.switchTo().window(windowHandles.get(0));
  }

  public static void device_report_should_display_for_the_share_link() throws Exception {
    AIQXUNS_Object.DiscoverTab.click();
  }

  public static void
      i_validate_that_device_type_showing_in_thumbnail_rreport_will_have_the_positive_index_in_the_detail_screen()
          throws Exception {
    final int totalBars = AIQXUNS_Object.UnsDeviceBarChart.size();
    Actions action = new Actions(driver);
    action.moveToElement(AIQXUNS_Object.UnsDeviceBarChart.get(0)).build().perform();

    for (int i = 0; i < (totalBars / 2); i++) {
      action.moveToElement(AIQXUNS_Object.UnsDeviceBarChart.get(i)).build().perform();
      final String device =
          AIQXUNS_Object.UnsDeviceReportTooltipHeader.getText()
              .substring(AIQXUNS_Object.UnsDeviceReportTooltipHeader.getText().indexOf(":") + 1);
      final String index =
          AIQXUNS_Object.UnsDeviceReportTooltipIndex.getText()
              .substring(AIQXUNS_Object.UnsDeviceReportTooltipIndex.getText().indexOf(":") + 1);

      if (Double.parseDouble(index) > 0) {
        if (!positiveIndexDevices.containsKey(device.trim().toLowerCase())) {
          assertTrue(false);
        }
      }
    }
    AIQXUNS_Object.DiscoverTab.click();
  }
}
