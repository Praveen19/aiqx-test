package com.mediaiq.aiqx.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Runner Class for all the test cases for UNSAutocomplete feature file and its implementation in
 * Step Definition
 */
@RunWith(Cucumber.class)
@CucumberOptions(
  features = {"classpath:features/UNSAutocomplete.feature"},
  glue = "classpath:com.mediaiq.aiqx.test.step_definitions",
  plugin = {"pretty", "html:target/cucumber-html-report", "json:target/Cucumber.json"},
  tags = {},
  monochrome = true
)
public class RunAiqxUnsAutoCompleteTest {}
