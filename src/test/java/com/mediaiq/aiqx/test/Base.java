package com.mediaiq.aiqx.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.mediaiq.aiqx.helpers.DataHelper;
import com.mediaiq.aiqx.helpers.Log;
import com.mediaiq.aiqx.test.pageobjects.LoginPageObject;
import com.mediaiq.aiqx.test.step_definitions.Hooks;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class Base {

  public static WebDriver driver;
  public static boolean bResult;
  public static String SA;

  public static Connection redshiftconnection;
  public static JavascriptExecutor jse = (JavascriptExecutor) driver;
  public static LoginPageObject loginPage =
      PageFactory.initElements(Hooks.driver, LoginPageObject.class);

  public static final WebDriverWait wait = new WebDriverWait(driver, 30);

  public Base(WebDriver driver) {
    Base.driver = driver;
    Base.bResult = true;
  }

  public static void clear(WebElement element) {}

  public static String readWorkBook(String workBookName, String sheetName, int row, int column)
      throws IOException {
    return DataHelper.readWorkBook(
        DataHelper.projectLocation, workBookName, sheetName, row, column);
  }

  /** Code to handle multiple browser window */
  public static String CreateParentWindowHandle(WebDriver driver, String winHandleBefore) {

    winHandleBefore = driver.getWindowHandle();

    return winHandleBefore;
  }

  /** code to check the element present in UI */
  public static boolean isElementPresent(WebElement locatorKey) {
    try {
      locatorKey.isDisplayed();
      return true;
    } catch (org.openqa.selenium.NoSuchElementException e) {
      return false;
    }
  }

  /** code to take the screenshot for success case */
  public static void takeScreenShot() throws IOException {
    final File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

    DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    Calendar cal = Calendar.getInstance();
    Log.info(dateFormat.format(cal.getTime()));

    String scrFilepath = scrFile.getAbsolutePath();
    Log.info("scrFilepath: " + scrFilepath);

    File currentDirFile = new File("Screenshots");
    String path = currentDirFile.getAbsolutePath();
    Log.info("path: " + path + "+++");

    Log.info(
        "****\n" + path + "\\screenshot" + dateFormat.format(cal.getTime()) + ".png");

    FileUtils.copyFile(
        scrFile, new File(path + "\\screenshot" + dateFormat.format(cal.getTime()) + ".png"));
  }

  /** Stale Element error */
  public static void staleElement(List<WebElement> entityTabs) {
    boolean breakIt = true;
    while (true) {
      breakIt = true;
      try {
        wait.until(ExpectedConditions.visibilityOfAllElements(entityTabs));
      } catch (Exception e) {
        if (e.getMessage().contains("element is not attached")) {
          breakIt = false;
        }
      }
      if (breakIt) {
        break;
      }
    }
  }

  /** Code to fetch the similar advertiser for any brand HTTP POST request */
  public static void sendPost(String brandname, String geo) throws Exception {

    String serveraddress = "http://52.90.101.136:5900/user_query_";
    // URL obj = new URL(se);
    // HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
    URL url = new URL(serveraddress);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();

    // add request header
    con.setRequestMethod("POST");

    con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
    // con.setRequestProperty("User-Agent", USER_AGENT);
    con.setRequestProperty("Content-Type", "application/json");
    con.setRequestProperty("Accept", "application/json");

    String urlParameters =
        "{\"query\":\""
            + brandname
            + "\",\"country\":\""
            + geo
            + "\",\"intent\":\"advertiser\",\"maxResults\":10}";

    // Send post request
    con.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
    wr.writeBytes(urlParameters);
    wr.flush();
    wr.close();

    int responseCode = con.getResponseCode();
    Log.info("\nSending 'POST' request to URL : " + url);
    Log.info("Post parameters : " + urlParameters);
    Log.info("Response Code : " + responseCode);

    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    String inputLine;
    StringBuffer response = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      response.append(inputLine);
    }
    in.close();

    Log.info(response.toString());

    JSONObject json = new JSONObject(response.toString());
    JSONArray jsonarray = json.getJSONArray("results");

    String[] arr = new String[jsonarray.length()];
    for (int i = 0; i < jsonarray.length(); i++) {
      JSONObject obj = jsonarray.getJSONObject(i);
      arr[i] = String.valueOf(obj.getInt("advertiser_id"));
    }
    SA = String.join(",", arr);
    Log.info("Final Similar Advertiser: " + SA);
  }

  public static void analyzeLog() {
    LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
    for (LogEntry entry : logEntries) {

      Log.info(entry.getMessage());
    }
  }

  /** Below function is used to check whether list contains the given string or not */
  public static boolean ContainString(String GivenString, List<WebElement> ListOfStrings) {
    for (WebElement we : ListOfStrings) {
      if (GivenString != null && we != null && GivenString.equalsIgnoreCase(we.getText())) {
        return true;
      }
    }
    return false;
  }

  /** isFileDownloaded function will be used to check the exact name of the download file */
  public static boolean isFileDownloaded(String downloadPath, String fileName) {
    boolean flag = false;
    File dir = new File(downloadPath);
    File[] dir_contents = dir.listFiles();

    for (int i = 0; i < dir_contents.length; i++) {
      if (dir_contents[i].getName().equals(fileName)) return flag = true;
    }

    return flag;
  }

  /**
   * isFileDownloaded_Ext function will be used to check the download file has specific extension or
   * not
   */
  public static boolean isFileDownloaded_Ext(
      String entityName, String reportType, String dirPath, String ext) {

    boolean flag = false;
    File dir = new File(dirPath);
    File[] files = dir.listFiles();
    if (files == null || files.length == 0) {
      return false;
    }
    for (int i = 0; i < files.length; i++) {
      if (StringUtils.containsIgnoreCase(
          files[i].getName(), entityName + "_" + reportType + "Report")) {
        flag = true;
        files[i].delete();
      }
    }
    return flag;
  }

  /** Below function will be used to switch between two tabs in chrome browser */
  public static void switchToTab() {
    // Switching between tabs using CTRL + tab keys.
    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "\t");
    // Switch to current selected tab's content.
    driver.switchTo().defaultContent();
  }
}
