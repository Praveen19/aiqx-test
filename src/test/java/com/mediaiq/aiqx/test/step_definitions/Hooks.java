package com.mediaiq.aiqx.test.step_definitions;

import com.mediaiq.aiqx.helpers.Log;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import com.mediaiq.aiqx.utils.ConfigHelper;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.mediaiq.aiqx.test.constants.UNSConstants;
import com.mediaiq.aiqx.enums.Browser;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

/** This Hooks class is used for browser instance */
public class Hooks {

  public static WebDriver driver;
  public static Browser browser;

  private void setup() {
    String driver_type = ConfigHelper.INSTANCE.getValueFromConfig("driver_type");
    browser = Browser.valueOf(driver_type);
  }

  @Before("@Start")
  /** Delete all cookies at the start of each scenario to avoid shared state between tests */
  public void init() throws IOException {
    setup();

    switch (browser) {
      case CHROME:
        Log.info("Called openBrowser");
        if (System.getProperty("os.name").contains("Windows")) {
          /** For windows Machine */
          System.setProperty("webdriver.chrome.driver", "Drivers//chromedriver.exe");
        } else {
          /** For Linux Machine */
          Log.info("For Linux, opening Chrome");

          System.setProperty("webdriver.chrome.driver", "Drivers//chromedriver");
        }
        HashMap<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.prompt_for_download", "false");
        prefs.put("profile.default_content_settings.popups", 0);
        prefs.put(
            "download.default_directory",
            System.getProperty("user.dir") + File.separator + UNSConstants.downloadFilepath);
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("--test-type");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-web-security");
        options.addArguments("--no-proxy-server");
        options.addArguments("--js-flags=--expose-gc");
        options.addArguments("--enable-precise-memory-info");
        options.addArguments("--disable-popup-blocking");
        options.addArguments("--disable-default-apps");
        options.addArguments("disable-infobars");
        options.addArguments("--start-maximized");
        DesiredCapabilities caps = DesiredCapabilities.chrome();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);
        caps.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(ChromeOptions.CAPABILITY, options);
        caps.setCapability("chrome.setProxyByServer", false);

        driver = new ChromeDriver(caps);
        break;
      case FIREFOX:
        Log.info("Called openBrowser");
        if (System.getProperty("os.name").contains("Windows")) {
          /** For windows Machine */
          System.setProperty("webdriver.gecko.driver", "Drivers//geckodriver.exe");
        } else {
          /** For Linux Machine */
          Log.info("For Linux, opening Chrome");

          System.setProperty("webdriver.gecko.driver", "Drivers//geckodriver");
        }
        driver = new FirefoxDriver();
        break;
      case INTERNET_EXPLORER:
        break;
      default:
        break;
    }
    openBrowser();
  }

  /** Resize current window to the set dimension */
  public void resizeBrowser() {
    Dimension d = new Dimension(1920, 1080);
    driver.manage().window().setSize(d);
  }

  public void openBrowser() {
    // resizeBrowser();
    driver.manage().window().maximize();
    driver.manage().deleteAllCookies();
    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
  }

  @After("@NegativeTest")
  public void beforeScenario(Scenario scenario) {}

  @After
  /** Embed a screenshot in test report if test is marked as failed */
  public void embedScreenshot(Scenario scenario) {

    if (scenario.isFailed()) {
      try {
        scenario.write("Current Page URL is " + driver.getCurrentUrl());
        byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
      } catch (WebDriverException somePlatformsDontSupportScreenshots) {
        System.err.println(somePlatformsDontSupportScreenshots.getMessage());
      }
    }
  }

  @After("@End")
  /** Embed a screenshot in test report if test is marked as failed */
  public void embedScreenshotCloseBrowser(Scenario scenario) {

    embedScreenshot(scenario);
    driver.close();
  }
}
