package com.mediaiq.aiqx.test.step_definitions;

import com.mediaiq.aiqx.test.modules.*;

import org.openqa.selenium.WebDriver;
import com.mediaiq.aiqx.enums.Browser;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class TestStepsAIQXHome {

  private WebDriver driver;
  private Browser browser;
  private static String brandName;
  private static String Geo;

  public TestStepsAIQXHome() {
    driver = Hooks.driver;
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void setDriver(WebDriver driver) {
    this.driver = driver;
  }

  public Browser getBrowser() {
    return browser;
  }

  public void setBrowser(Browser browser) {
    this.browser = browser;
  }

  public static String brandName() {
    return brandName;
  }

  public static String geo() {
    return Geo;
  }

  @Given("^I navigate to the aiqx URL$")
  public void i_navigate_to_the_aiqx_URL() throws Throwable {
    AiqxLogin.i_navigate_to_the_aiqx_URL();
  }

  @And("^I enter invalid (.+) and (.+) in the login page and click on Sign In button$")
  public void i_enter_invalid_and_in_the_login_page_and_click_on_sign_in_button(
      String username, String password) throws Throwable {
    AiqxLogin.i_enter_invalid_and_in_the_login_page_and_click_on_sign_in_button(username, password);
  }

  @Then("^I check the validation message in the logiin page$")
  public void i_check_the_validation_message_in_the_logiin_page() throws Throwable {
    AiqxLogin.i_check_the_validation_message_in_the_logiin_page();
  }

  @And("^I enter the Username as (.+) and  Password as (.+) and click on Sign in button$")
  public void i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
      String username, String password) throws Throwable {
    AiqxLogin.i_enter_the_username_as_and_password_as_and_click_on_sign_in_button(
        username, password);
  }

  @Then("^I verify the aiqx Landing Discover title and logged username as (.+)$")
  public void i_verify_the_aiqx_landing_discover_title_and_logged_username_as(String username)
      throws Throwable {
    AiqxLogin.i_verify_the_aiqx_landing_discover_title_and_logged_username_as(username);
  }

  @And("^Enter the  (.+) and  (.+) and click on Sign in button$")
  public void enter_the_and_and_click_on_sign_in_button(String username, String password)
      throws Throwable {
    AiqxLogin.enter_the_and_and_click_on_sign_in_button(username, password);
  }

  @Then("^Aiqx Landing Discover title and logged username as (.+)$")
  public void aiqx_landing_discover_title_and_logged_username_as(String username) throws Throwable {

    AiqxLogin.aiqx_landing_discover_title_and_logged_username_as(username);
  }

  @Then("^Click on Logout$")
  public void click_on_Logout() throws Throwable {
    AiqxLogin.click_on_Logout();
  }

  // AutoComplete Functionlaity

  @And("^I verify that search box is auto focus$")
  public void i_verify_that_search_box_is_auto_focus() throws Throwable {
    AiqxUns.i_verify_that_search_box_is_auto_focus();
  }

  @And("^I check the menu icon bar is clickable$")
  public void i_check_the_menu_icon_bar_is_clickable() throws Throwable {
    AiqxUns.i_check_the_menu_icon_bar_is_clickable();
  }

  @And("^I verify the menu list items$")
  public void i_verify_the_menu_list_items() throws Throwable {
    AiqxUns.i_verify_the_menu_list_items();
  }

  @And("^I verify the Discover text should present in the landing screen$")
  public void i_verify_the_discover_text_should_present_in_the_landing_screen() throws Throwable {
    AiqxUns.i_verify_the_discover_text_should_present_in_the_landing_screen();
  }

  @Given("^I enter the substring as (.+) in search box of the laanding screen$")
  public void i_enter_the_substring_as_in_search_box_of_the_laanding_screen(String substring)
      throws Throwable {
    AiqxUns.i_enter_the_substring_as_in_search_box_of_the_laanding_screen(substring);
  }

  @And("^I validate that the autosuggest results should contains the searched substring as (.+)$")
  public void i_validate_that_the_autosuggest_results_should_contains_the_searched_substring_as(
      String substring) throws Throwable {

    AiqxUns.i_validate_that_the_autosuggest_results_should_contains_the_searched_substring_as(
        substring);
  }

  @And("^I validate that at max only twenty four box should show in the autosuggest$")
  public void i_validate_that_at_max_only_twenty_four_box_should_show_in_the_autosuggest()
      throws Throwable {
    AiqxUns.i_validate_that_at_max_only_twenty_four_box_should_show_in_the_autosuggest();
  }

  @Given("^I enter the exact search entity as (.+) in search box of report landing screen$")
  public void i_enter_the_exact_search_entity_as_in_search_box_of_report_landing_screen(
      String exactstring) throws Throwable {
    AiqxUns.i_enter_the_exact_search_entity_as_in_search_box_of_report_landing_screen(exactstring);
  }

  @And("^I validate that the autosuggest results should contains the exact search string as (.+)$")
  public void i_validate_that_the_autosuggest_results_should_contains_the_exact_search_string_as(
      String exactstring) throws Throwable {
    AiqxUns.i_validate_that_the_autosuggest_results_should_contains_the_exact_search_string_as(
        exactstring);
  }

  @Given("^I enter the any valid entity id as (.+) in search box of report landing screen$")
  public void i_enter_the_any_valid_entity_id_as_in_search_box_of_report_landing_screen(
      String validid) throws Throwable {
    AiqxUns.i_enter_the_any_valid_entity_id_as_in_search_box_of_report_landing_screen(validid);
  }

  @And(
      "^I validate that autosuggest should only display the searched result name in auto suggestion$")
  public void
      i_validate_that_autosuggest_should_only_display_the_searched_result_name_in_auto_suggestion()
          throws Throwable {
    AiqxUns
        .i_validate_that_autosuggest_should_only_display_the_searched_result_name_in_auto_suggestion();
  }

  @Given("^I enter the any valid string as (.+) in search box of landing screen$")
  public void i_enter_the_any_valid_string_as_in_search_box_of_landing_screen(String validstring)
      throws Throwable {
    AiqxUns.i_enter_the_any_valid_string_as_in_search_box_of_landing_screen(validstring);
  }

  @And(
      "^I validate the number of count of results in autosuggest screen should match with the count of results in landing page$")
  public void
      i_validate_the_number_of_count_of_results_in_autosuggest_screen_should_match_with_the_count_of_results_in_landing_page()
          throws Throwable {
    AiqxUns
        .i_validate_the_number_of_count_of_results_in_autosuggest_screen_should_match_with_the_count_of_results_in_landing_page();
  }

  @And(
      "^I validate the counts of categories showing in the landing screen should match with the number of categories tabs$")
  public void
      i_validate_the_counts_of_categories_showing_in_the_landing_screen_should_match_with_the_number_of_categories_tabs()
          throws Throwable {
    AiqxUns
        .i_validate_the_counts_of_categories_showing_in_the_landing_screen_should_match_with_the_number_of_categories_tabs();
  }

  @And("^I validate that all the categories tabs are clickable$")
  public void i_validate_that_all_the_categories_tabs_are_clickable() throws Throwable {
    AiqxUns.i_validate_that_all_the_categories_tabs_are_clickable();
  }

  @And(
      "^I validate that results in all the categories tabs are equal to the count of results in respective cetagories tab$")
  public void
      i_validate_that_results_in_all_the_categories_tabs_are_equal_to_the_count_of_results_in_respective_cetagories_tab()
          throws Throwable {
    AiqxUns
        .i_validate_that_results_in_all_the_categories_tabs_are_equal_to_the_count_of_results_in_respective_cetagories_tab();
  }

  // UNS Keyword Report
  @Given("^I triger the below test step from inititate function with the given (.+) and (.+)$")
  public void i_triger_the_below_test_step_from_inititate_function_with_the_given_and(
      String entityname, String entitytype) throws Throwable {
    AiqxKeyword.i_triger_the_below_test_step_from_inititate_function_with_the_given_and(
        entityname, entitytype);
  }

  @And("^I click and set the geo as (.+)$")
  public void i_click_and_set_the_geo_as(String geo) throws Throwable {

    AiqxKeyword.i_click_and_set_the_geo_as(geo);
  }

  @And("^I enter the valid entity name as (.+) in search box of app landing screen$")
  public void i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(
      String entityname) throws Throwable {
    AiqxKeyword.i_enter_the_valid_entity_name_as_in_search_box_of_app_landing_screen(entityname);
  }

  @And("^I verify the entity type as (.+) in category tab$")
  public void i_verify_the_entity_type_as_in_category_tab(String entitytype) throws Throwable {
    AiqxKeyword.i_verify_the_entity_type_as_in_category_tab(entitytype);
  }

  @And(
      "^I click on the respective categories tab of matching (.+) and click on the searched entity  from autocomplete screen$")
  public void
      i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
          String entitytype) throws Throwable {
    AiqxKeyword
        .i_click_on_the_respective_categories_tab_of_matching_and_click_on_the_searched_entity_from_autocomplete_screen(
            entitytype);
  }

  @And("^I validate that keyword Thumbnail report should appear in the report loading screen$")
  public void i_validate_that_keyword_thumbnail_report_should_appear_in_the_report_loading_screen()
      throws Throwable {
    AiqxKeyword
        .i_validate_that_keyword_thumbnail_report_should_appear_in_the_report_loading_screen();
  }

  @Given(
      "^I enter the valid entity name as (.+) of its entity type as (.+) in search box of landing screen$")
  public void
      i_enter_the_valid_entity_name_as_of_its_entity_type_as_in_search_box_of_landing_screen(
          String entityname, String entitytype) throws Throwable {
    AiqxKeyword
        .i_enter_the_valid_entity_name_as_of_its_entity_type_as_in_search_box_of_landing_screen(
            entityname, entitytype);
  }

  @And("^I click on the keyword report thumnail$")
  public void i_click_on_the_keyword_report_thumnail() throws Throwable {
    AiqxKeyword.i_click_on_the_keyword_report_thumnail();
  }

  @Then("^I validate the keyword report title$")
  public void i_validate_the_keyword_report_title() throws Throwable {
    AiqxKeyword.i_validate_the_keyword_report_title();
  }

  @Given(
      "^I should be on the Keyword treemap detail report screen for the searched (.+) of its type as (.+)$")
  public void
      i_should_be_on_the_keyword_treemap_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    AiqxKeyword
        .i_should_be_on_the_keyword_treemap_detail_report_screen_for_the_searched_of_its_type_as(
            entityname, entitytype);
  }

  @And("^All Categories button should be clickable$")
  public void all_categories_button_should_be_clickable() throws Throwable {
    AiqxKeyword.all_categories_button_should_be_clickable();
  }

  @And("^All categories should not contains Blacklist as category$")
  public void all_categories_should_not_contains_blacklist_as_category() throws Throwable {
    AiqxKeyword.all_categories_should_not_contains_blacklist_as_category();
  }

  @And("^View button tabs should be clickable$")
  public void view_button_tabs_should_be_clickable() throws Throwable {
    AiqxKeyword.view_button_tabs_should_be_clickable();
  }

  @And("^I click on tabular view button$")
  public void i_click_on_tabular_view_button() throws Throwable {
    AiqxKeyword.i_click_on_tabular_view_button();
  }

  @Then("^Tabular Chart should display$")
  public void tabular_chart_should_display() throws Throwable {
    AiqxKeyword.tabular_chart_should_display();
  }

  @Given(
      "^I should be on the Keyword tabular detail report screen for the searched (.+) of its type as (.+)$")
  public void
      i_should_be_on_the_keyword_tabular_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    AiqxKeyword
        .i_should_be_on_the_keyword_tabular_detail_report_screen_for_the_searched_of_its_type_as(
            entityname, entitytype);
  }

  @And(
      "^Performance index value on the keyword tabular detail report screen should be between minus one to plus one$")
  public void
      performance_index_value_on_the_keyword_tabular_detail_report_screen_should_be_between_minus_one_to_plus_one()
          throws Throwable {
    AiqxKeyword
        .performance_index_value_on_the_keyword_tabular_detail_report_screen_should_be_between_minus_one_to_plus_one();
  }

  @And("^I click on any category from the tabular chart$")
  public void i_click_on_any_category_from_the_tabular_chart() throws Throwable {
    AiqxKeyword.i_click_on_any_category_from_the_tabular_chart();
  }

  @And("^Check all the keywords sort with same clicked category$")
  public void check_all_the_keywords_sort_with_same_clicked_category() throws Throwable {
    AiqxKeyword.check_all_the_keywords_sort_with_same_clicked_category();
  }

  @And("^All categories button shoud display the same clicked category name$")
  public void all_categories_button_shoud_display_the_same_clicked_category_name()
      throws Throwable {
    AiqxKeyword.all_categories_button_shoud_display_the_same_clicked_category_name();
  }

  @And("^I click on the treemap button from the keyword tabular chart screen$")
  public void i_click_on_the_treemap_button_from_the_keyword_tabular_chart_screen()
      throws Throwable {
    AiqxKeyword.i_click_on_the_treemap_button_from_the_keyword_tabular_chart_screen();
  }

  @And("^I click on the All ategories button$")
  public void i_click_on_the_all_ategories_button() throws Throwable {
    AiqxKeyword.i_click_on_the_all_ategories_button();
  }

  @And("^I select All categories filter$")
  public void i_select_all_categories_filter() throws Throwable {
    AiqxKeyword.i_select_all_categories_filter();
  }

  @And("^I click on download as Image button$")
  public void i_click_on_download_as_image_button() throws Throwable {
    AiqxKeyword.i_click_on_download_as_image_button();
  }

  @Then("^Report gets downloaded of its type as (.+)$")
  public void report_gets_downloaded_of_its_type_as(String reportType) throws Throwable {
    AiqxKeyword.report_gets_downloaded(reportType);
  }

  @And("^I click on the Export Report button$")
  public void i_click_on_the_export_report_button() throws Throwable {
    AiqxKeyword.i_click_on_the_export_report_button();
  }

  @And("^I copied the share link$")
  public void i_copied_the_share_link() throws Throwable {
    AiqxKeyword.i_copied_the_share_link();
  }

  @And("^I browse the copied share link in any browser tab$")
  public void i_browse_the_copied_share_link_in_any_browser_tab() throws Throwable {
    AiqxKeyword.i_browse_the_copied_share_link_in_any_browser_tab();
  }

  @Then("^Keyword Report should display for the share link$")
  public void keyword_report_should_display_for_the_share_link() throws Throwable {
    AiqxKeyword.keyword_report_should_display_for_the_share_link();
  }

  @And("^Like/Dislike button should be clickable$")
  public void likedislike_button_should_be_clickable() throws Throwable {
    AiqxKeyword.likedislike_button_should_be_clickable();
  }

  @Given(
      "^I enter the valid keyword as (.+) of its entity type as (.+) in search box of landing screen$")
  public void i_enter_the_valid_keyword_as_of_its_entity_type_as_in_search_box_of_landing_screen(
      String entityname, String entitytype) throws Throwable {
    AiqxKeyword.i_enter_the_valid_keyword_as_of_its_entity_type_as_in_search_box_of_landing_screen(
        entityname, entitytype);
  }

  @Then(
      "^Duplicate keywords should not display in the autocomplete for the searched entityname as (.+) in its respective tab of its type as  (.+)$")
  public void
      duplicate_keywords_should_not_display_in_the_autocomplete_for_the_searched_entityname_as_in_its_respective_tab_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    AiqxKeyword
        .duplicate_keywords_should_not_display_in_the_autocomplete_for_the_searched_entityname_as_in_its_respective_tab_of_its_type_as(
            entityname, entitytype);
  }

  /** UNS Temporal Trends Report */
  @And("^I validate that Temporal Thumbnail report should appear in the report loading screen$")
  public void i_validate_that_temporal_thumbnail_report_should_appear_in_the_report_loading_screen()
      throws Throwable {

    AiqxTemporal
        .i_validate_that_temporal_thumbnail_report_should_appear_in_the_report_loading_screen();
  }

  @And("^I click on the temporal report thumbnail$")
  public void i_click_on_the_temporal_report_thumbnail() throws Throwable {

    AiqxTemporal.i_click_on_the_temporal_report_thumbnail();
  }

  @Then("^I validate the temporal report title$")
  public void i_validate_the_temporal_report_title() throws Throwable {

    AiqxTemporal.i_validate_the_temporal_report_title();
  }

  @Given(
      "^I should be on the Temporal trends detail report screen for the searched (.+) of its type as (.+)$")
  public void
      i_should_be_on_the_temporal_trends_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {

    AiqxTemporal
        .i_should_be_on_the_temporal_trends_detail_report_screen_for_the_searched_of_its_type_as(
            entityname, entitytype);
  }

  @And("^TOD button should be clickable$")
  public void tod_button_should_be_clickable() throws Throwable {

    AiqxTemporal.tod_button_should_be_clickable();
  }

  @And("^DOW button should be clicakble$")
  public void dow_button_should_be_clicakble() throws Throwable {

    AiqxTemporal.dow_button_should_be_clicakble();
  }

  @And("^Hours axis should have value between zero to twentythree$")
  public void hours_axis_should_have_value_between_zero_to_twentythree() throws Throwable {

    AiqxTemporal.hours_axis_should_have_value_between_zero_to_twentythree();
  }

  @And("^Hour axis should have no duplicate hour value$")
  public void hour_axis_should_have_no_duplicate_hour_value() throws Throwable {

    AiqxTemporal.hour_axis_should_have_no_duplicate_hour_value();
  }

  @And("^I click on discover breadcrumb tab to go discover page$")
  public void i_click_on_discover_breadcrumb_tab_to_go_discover_page() throws Throwable {
    AiqxTemporal.i_click_on_discover_breadcrumb_tab_to_go_discover_page();
  }

  @And("^I click on Day of Week button$")
  public void i_click_on_day_of_week_button() throws Throwable {

    AiqxTemporal.i_click_on_day_of_week_button();
  }

  @And("^Day axis should be in sorted order$")
  public void day_axis_should_be_in_sorted_order() throws Throwable {

    AiqxTemporal.day_axis_should_be_in_sorted_order();
  }

  @And("^I copied the share link of temporal trend report$")
  public void i_copied_the_share_link_of_temporal_trend_report() throws Throwable {

    AiqxTemporal.i_copied_the_share_link_of_temporal_trend_report();
  }

  @And("^I browsed the copied share link of temporal report in any browser tab$")
  public void i_browsed_the_copied_share_link_of_temporal_report_in_any_browser_tab()
      throws Throwable {
    AiqxTemporal.i_browsed_the_copied_share_link_of_temporal_report_in_any_browser_tab();
  }

  @Then("^Temporal Report should display for the share link$")
  public void temporal_report_should_display_for_the_share_link() throws Throwable {

    AiqxTemporal.temporal_report_should_display_for_the_share_link();
  }

  // Audience Report

  @Given("^I click and set the (.+) from geo picker$")
  public void i_click_and_set_the_from_geo_picker(String geo) throws Throwable {
    AiqxAudience.i_click_and_set_the_from_geo_picker(geo);
  }

  @And("^I click on the audience report thumbnail$")
  public void i_click_on_the_audience_report_thumbnail() throws Throwable {
    AiqxAudience.i_click_on_the_audience_report_thumbnail();
  }

  @And("^I validate that Audience Thumbnail report should appear in the report loading screen$")
  public void i_validate_that_audience_thumbnail_report_should_appear_in_the_report_loading_screen()
      throws Throwable {
    AiqxAudience
        .i_validate_that_audience_thumbnail_report_should_appear_in_the_report_loading_screen();
  }

  @Given(
      "^I should be on the Audience trends detail report screen for the searched (.+) of its type as (.+)$")
  public void
      i_should_be_on_the_audience_trends_detail_report_screen_for_the_searched_of_its_type_as(
          String entityname, String entitytype) throws Throwable {
    AiqxAudience
        .i_should_be_on_the_audience_trends_detail_report_screen_for_the_searched_of_its_type_as(
            entityname, entitytype);
  }

  @And("^Segment Name should be unique$")
  public void segment_name_should_be_unique() throws Throwable {
    AiqxAudience.segment_name_should_be_unique();
  }

  @Then("^I validate the audience report title$")
  public void i_validate_the_audience_report_title() throws Throwable {
    AiqxAudience.i_validate_the_audience_report_title();
  }

  @And("^Brand Name should be there for each segment name$")
  public void brand_name_should_be_there_for_each_segment_name() throws Throwable {
    AiqxAudience.brand_name_should_be_there_for_each_segment_name();
  }

  @And("^Index value in Audience report should lie between minus one to plus one$")
  public void index_value_in_audience_report_should_lie_between_minus_one_to_plus_one()
      throws Throwable {
    AiqxAudience.index_value_in_audience_report_should_lie_between_minus_one_to_plus_one();
  }

  @And("^I copied the share link of audience trend report$")
  public void i_copied_the_share_link_of_audience_trend_report() throws Throwable {
    AiqxAudience.i_copied_the_share_link_of_audience_trend_report();
  }

  @And("^I browsed the copied share link of audience report in any browser tab$")
  public void i_browsed_the_copied_share_link_of_audience_report_in_any_browser_tab()
      throws Throwable {
    AiqxAudience.i_browsed_the_copied_share_link_of_audience_report_in_any_browser_tab();
  }

  @Then("^Audience Report should display for the share link$")
  public void audience_report_should_display_for_the_share_link() throws Throwable {
    AiqxAudience.audience_report_should_display_for_the_share_link();
  }

  @And("^I validate that Geo Thumbnail report should appear in the report loading screen$")
  public void i_validate_that_geo_thumbnail_report_should_appear_in_the_report_loading_screen()
      throws Throwable {
    AiqxGeoPerformance
        .i_validate_that_geo_thumbnail_report_should_appear_in_the_report_loading_screen();
  }

  @Then("^I validate the city names present in the geo thumbnail report with its user geo$")
  public void i_validate_the_city_names_present_in_the_geo_thumbnail_report_with_its_user_geo()
      throws Throwable {
    AiqxGeoPerformance
        .i_validate_the_city_names_present_in_the_geo_thumbnail_report_with_its_user_geo();
  }

  @And("^I click on the geo report thumbnail$")
  public void i_click_on_the_geo_report_thumbnail() throws Throwable {
    AiqxGeoPerformance.i_click_on_the_geo_report_thumbnail();
  }

  @Then("^I validate the Geo report title$")
  public void i_validate_the_geo_report_title() throws Throwable {
    AiqxGeoPerformance.i_validate_the_geo_report_title();
  }

  @Given("^I should be on the geo detail report screen for the searched (.+) of its type as (.+)$")
  public void i_should_be_on_the_geo_detail_report_screen_for_the_searched_of_its_type_as(
      String entityname, String entitytype) throws Throwable {
    AiqxGeoPerformance.i_should_be_on_the_geo_detail_report_screen_for_the_searched_of_its_type_as(
        entityname, entitytype);
  }

  @And("^I copied the share link of geo trend report$")
  public void i_copied_the_share_link_of_geo_trend_report() throws Throwable {
    AiqxGeoPerformance.i_copied_the_share_link_of_geo_trend_report();
  }

  @Then("^Geo Report should display for the share link$")
  public void geo_report_should_display_for_the_share_link() throws Throwable {
    AiqxGeoPerformance.geo_report_should_display_for_the_share_link();
  }

  @And("^I browsed the copied share link of geo report in any browser tab$")
  public void i_browsed_the_copied_share_link_of_geo_report_in_any_browser_tab() throws Throwable {
    AiqxGeoPerformance.i_browsed_the_copied_share_link_of_geo_report_in_any_browser_tab();
  }

  @And(
      "^I validate that Device Ownership Thumbnail report should appear in the report loading screen$")
  public void
      i_validate_that_device_ownership_thumbnail_report_should_appear_in_the_report_loading_screen()
          throws Throwable {
    AiqxDevice
        .i_validate_that_device_ownership_thumbnail_report_should_appear_in_the_report_loading_screen();
  }

  @And("^I click on the device ownership report thumbnail$")
  public void i_click_on_the_device_ownership_report_thumbnail() throws Throwable {
    AiqxDevice.i_click_on_the_device_ownership_report_thumbnail();
  }

  @Then("^I validate the device Ownership report title$")
  public void i_validate_the_device_ownership_report_title() throws Throwable {
    AiqxDevice.i_validate_the_device_ownership_report_title();
  }

  @And("^I check the name of the device type from the device thumbnail report$")
  public void i_check_the_name_of_the_device_type_from_the_device_thumbnail_report()
      throws Throwable {
    AiqxDevice.i_check_the_name_of_the_device_type_from_the_device_thumbnail_report();
  }

  @Given(
      "^I should be on the device detail report screen for the searched (.+) of its type as (.+)$")
  public void i_should_be_on_the_device_detail_report_screen_for_the_searched_of_its_type_as(
      String entityname, String entitytype) throws Throwable {
    AiqxDevice.i_should_be_on_the_device_detail_report_screen_for_the_searched_of_its_type_as(
        entityname, entitytype);
  }

  @Then("^Device Report should display for the share link$")
  public void device_report_should_display_for_the_share_link() throws Throwable {
    AiqxDevice.device_report_should_display_for_the_share_link();
  }

  @And("^I copied the share link of device ownership report$")
  public void i_copied_the_share_link_of_device_ownership_report() throws Throwable {
    AiqxDevice.i_copied_the_share_link_of_device_ownership_report();
  }

  @And("^I browsed the copied share link of device report in any browser tab$")
  public void i_browsed_the_copied_share_link_of_device_report_in_any_browser_tab()
      throws Throwable {
    AiqxDevice.i_browsed_the_copied_share_link_of_device_report_in_any_browser_tab();
  }

  @And(
      "^I validate that device type showing in thumbnail rreport will have the positive index in the detail screen$")
  public void
      i_validate_that_device_type_showing_in_thumbnail_rreport_will_have_the_positive_index_in_the_detail_screen()
          throws Throwable {
    AiqxDevice
        .i_validate_that_device_type_showing_in_thumbnail_rreport_will_have_the_positive_index_in_the_detail_screen();
  }

  @And("^I validate that Site Domain Thumbnail report should appear in the report loading screen$")
  public void
      i_validate_that_site_domain_thumbnail_report_should_appear_in_the_report_loading_screen()
          throws Throwable {
    AiqxSiteDomain
        .i_validate_that_site_domain_thumbnail_report_should_appear_in_the_report_loading_screen();
  }

  @And("^I click on the site domain report thumbnail$")
  public void i_click_on_the_site_domain_report_thumbnail() throws Throwable {
    AiqxSiteDomain.i_click_on_the_site_domain_report_thumbnail();
  }

  @Then("^I validate the site domain report title$")
  public void i_validate_the_site_domain_report_title() throws Throwable {
    AiqxSiteDomain.i_validate_the_site_domain_report_title();
  }

  @Given(
      "^I should be on the Site Domain detail report screen for the searched (.+) of its type as (.+)$")
  public void i_should_be_on_the_site_domain_detail_report_screen_for_the_searched_of_its_type_as(
      String entityname, String entitytype) throws Throwable {
    AiqxSiteDomain
        .i_should_be_on_the_site_domain_detail_report_screen_for_the_searched_of_its_type_as(
            entityname, entitytype);
  }

  @And("^No duplicate domains name should present$")
  public void no_duplicate_domains_name_should_present() throws Throwable {
    AiqxSiteDomain.no_duplicate_domains_name_should_present();
  }

  @And("^Site Domain category name should not be empty$")
  public void site_domain_category_name_should_not_be_empty() throws Throwable {
    AiqxSiteDomain.site_domain_category_name_should_not_be_empty();
  }

  @And("^All the filter checkbox option should display in detail screen$")
  public void all_the_filter_checkbox_option_should_display_in_detail_screen() throws Throwable {
    AiqxSiteDomain.all_the_filter_checkbox_option_should_display_in_detail_screen();
  }

  @And("^All category button should be enable$")
  public void all_category_button_should_be_enable() throws Throwable {
    AiqxSiteDomain.all_category_button_should_be_enable();
  }

  @And("^I click on Low bucket filter on detail screen$")
  public void i_click_on_low_bucket_filter_on_detail_screen() throws Throwable {
    AiqxSiteDomain.i_click_on_low_bucket_filter_on_detail_screen();
  }

  @And("^All domains should get filter with volume as low$")
  public void all_domains_should_get_filter_with_volume_as_low() throws Throwable {
    AiqxSiteDomain.all_domains_should_get_filter_with_volume_as_low();
  }

  @And("^I click on Medium bucket filter on detail screen$")
  public void i_click_on_medium_bucket_filter_on_detail_screen() throws Throwable {
    AiqxSiteDomain.i_click_on_medium_bucket_filter_on_detail_screen();
  }

  @And("^All domain should get filter with volume as medium$")
  public void all_domain_should_get_filter_with_volume_as_medium() throws Throwable {
    AiqxSiteDomain.all_domain_should_get_filter_with_volume_as_medium();
  }

  @And("^I click on High bucket filter with volume as high$")
  public void i_click_on_high_bucket_filter_with_volume_as_high() throws Throwable {
    AiqxSiteDomain.i_click_on_high_bucket_filter_with_volume_as_high();
  }

  @And("^All domain should filter with volume as high$")
  public void all_domain_should_filter_with_volume_as_high() throws Throwable {
    AiqxSiteDomain.all_domain_should_filter_with_volume_as_high();
  }

  @And("^I click on all categories button$")
  public void i_click_on_all_categories_button() throws Throwable {
    AiqxSiteDomain.i_click_on_all_categories_button();
  }

  @And("^I select any category from the all categories drop down box$")
  public void i_select_any_category_from_the_all_categories_drop_down_box() throws Throwable {
    AiqxSiteDomain.i_select_any_category_from_the_all_categories_drop_down_box();
  }

  @Then("^All the domains should get filter with the selected category$")
  public void all_the_domains_should_get_filter_with_the_selected_category() throws Throwable {
    AiqxSiteDomain.all_the_domains_should_get_filter_with_the_selected_category();
  }

  @And("^I copied the share link of Site Domain report$")
  public void i_copied_the_share_link_of_site_domain_report() throws Throwable {
    AiqxSiteDomain.i_copied_the_share_link_of_site_domain_report();
  }

  @And("^I browsed the copied share link of site domain report in any browser tab$")
  public void i_browsed_the_copied_share_link_of_site_domain_report_in_any_browser_tab()
      throws Throwable {
    AiqxSiteDomain.i_browsed_the_copied_share_link_of_site_domain_report_in_any_browser_tab();
  }

  @Then("^Site Domain Report should display for the share link$")
  public void site_domain_report_should_display_for_the_share_link() throws Throwable {
    AiqxSiteDomain.site_domain_report_should_display_for_the_share_link();
  }

  @Then("^All the domains should be clickable$")
  public void all_the_domains_should_be_clickable() throws Throwable {
    AiqxSiteDomain.all_the_domains_should_be_clickable();
  }

  @And("^I click on any site domain link$")
  public void i_click_on_any_site_domain_link() throws Throwable {
    AiqxSiteDomain.i_click_on_any_site_domain_link();
  }

  @And("^Link should be open in new tab$")
  public void link_should_be_open_in_new_tab() throws Throwable {
    AiqxSiteDomain.link_should_be_open_in_new_tab();
  }

  @And("^I close the newly opened tab$")
  public void i_close_the_newly_opened_tab() throws Throwable {
    AiqxSiteDomain.i_close_the_newly_opened_tab();
  }

  @And("^I click on discover breadcrum tab$")
  public void i_click_on_discover_breadcrum_tab() throws Throwable {
    AiqxSiteDomain.i_click_on_discover_breadcrum_tab();
  }
}
