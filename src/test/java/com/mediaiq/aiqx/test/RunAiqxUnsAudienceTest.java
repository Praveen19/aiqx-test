package com.mediaiq.aiqx.test;

import java.io.File;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Runner Class for all the test cases for UNSAudienceReport feature file and its implementation in
 * Step Definition
 */
@RunWith(Cucumber.class)
@CucumberOptions(
  features = {"classpath:features/UnsAudienceReport.feature"},
  glue = "classpath:com.mediaiq.aiqx.test.step_definitions",
  plugin = ("com.cucumber.listener.ExtentCucumberFormatter:target/Destination/report.html"),
  format = {"pretty", "html:target/Destination"}
)
public class RunAiqxUnsAudienceTest {
  @AfterClass
  public static void report() {
    Reporter.loadXMLConfig(new File("src/test/resources/extent.xml"));
  }
}
