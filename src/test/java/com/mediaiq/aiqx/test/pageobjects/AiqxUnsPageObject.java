package com.mediaiq.aiqx.test.pageobjects;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mediaiq.aiqx.test.Base;

/**
 * This class is used to store all the webelement position in Universal Search Implementation page
 */
public class AiqxUnsPageObject extends Base {

  public AiqxUnsPageObject(WebDriver driver) {
    super(driver);
    // TODO Auto-generated constructor stub
  }

  @FindBy(xpath = "//input[@type = 'text']")
  public WebElement SearchBox;

  @FindBy(xpath = "//div[@id = 'autoSuggest-tab']/ul/li/a")
  public List<WebElement> EntityTabs;

  @FindBy(xpath = "//div[@id = 'autoSuggest-tab-pane-all']/ul/li")
  public List<WebElement> AutosuggestBox;

  @FindBy(xpath = "//div[@class = 'panel panel-default container-fluid']/span[4]/span[1]")
  public List<WebElement> MatchingResultName;

  @FindBy(xpath = "//span[@class = 'highlight']")
  public List<WebElement> HighlightedResulttext;

  @FindBy(xpath = "//span[@class = 'name']")
  public WebElement SingleEntityResult;

  @FindBy(xpath = "//div[@id = 'autoSuggest-tab']/ul/li/a")
  public List<WebElement> AutoCompleteTabs;

  @FindBy(xpath = "//div[@class = 'search-result-count']/span[1]")
  public WebElement AutoCompleteResultCount;

  @FindBy(xpath = "//div[@class = 'search-result-count']/span[2]/span")
  public WebElement AutoCompleteCategoriesCount;

  @FindBy(xpath = "//div[@class = 'badge autosuggest-badge']/span")
  public WebElement SearchCategoryType;

  /** PageObject of Keyword Report */
  @FindBy(xpath = "//div[@id = 'keywordPanel_thumb']/div/div/a")
  public WebElement UNSKeyword_Thumbnail;

  @FindBy(xpath = "//span[@class = 'badge']")
  public WebElement Entitybadge;

  @FindBy(xpath = "//div[text() = 'Keyword Explorer']")
  public WebElement UNSKeywordReportTitle;

  @FindBy(xpath = "//a[text() = 'Discover']")
  public WebElement DiscoverTab;

  @FindBy(xpath = "//div[@class = 'dropdown-keyword']/div/button")
  public WebElement AllCategoriesButton;

  @FindBy(xpath = "//div[@class = 'dropdown open btn-group']/ul/li/a/div/div")
  public List<WebElement> AllCategoriesList;

  public WebElement ViewButton(int ButtonIndex) {
    By viewbutton =
        By.xpath("//div[@class = 'switch-button btn-group']/button[" + ButtonIndex + "]");
    return driver.findElement(viewbutton);
  }

  @FindBy(xpath = "//button[text() = 'Export report']")
  public WebElement ExportReportButton;

  @FindBy(xpath = "//div[text() = 'Image']")
  public WebElement DownloadAsImageButton;

  @FindBy(xpath = "//div[@class = 'share-url']/input")
  public WebElement ShareUrlLink;

  @FindBy(xpath = "//div[text() = 'Share Link:']")
  public WebElement ShareLinkBox;

  @FindBy(xpath = "//div[@class = 'pi-parent']/div[1]")
  public List<WebElement> PerformanceIndexvalue;

  @FindBy(xpath = "//div[@class = 'keyword-category']")
  public List<WebElement> KeywordCategory;

  @FindBy(xpath = "//div[contains(text() , 'INDEX')]")
  public WebElement KeywordPerfIndexHeader;

  @FindBy(xpath = "//tbody/tr[1]/td[2]/div[@class = 'keyword-category']")
  public WebElement FirstKeywordCategory;

  @FindBy(xpath = "//div[text() = 'All Categories']")
  public WebElement AllCategoryOption;

  @FindBy(xpath = "//div[@id = 'audiencePanel_thumb']/div/div/a")
  public WebElement UNSAudience_Thumbnail;

  @FindBy(xpath = "//div[@class = 'segment-name-div']")
  public List<WebElement> AudienceSegmentNames;

  @FindBy(xpath = "//div[@class='usn-brand-logo']")
  public List<WebElement> UNSAudienceBrandtext;

  @FindBy(xpath = "//div[text() = 'Audience Explorer']")
  public WebElement UNSAudienceReportTitle;

  public WebElement LikeDislikeButton(int ButtonIndex) {
    By likedislikebutton =
        By.xpath("//div[@class = 'like-dislike btn-group pull-right']/div[" + ButtonIndex + "]");
    return driver.findElement(likedislikebutton);
  }

  @FindBy(xpath = "//div[@class = 'panel panel-default container-fluid']/span[6]")
  public List<WebElement> CompleteMatchingAutoCompleteList;

  @FindBy(xpath = "//div[@class = 'panel panel-default container-fluid']/div/span[1]")
  public List<WebElement> MatchingAutoCompleteEntityTypeList;

  @FindBy(xpath = "//div[@class = 'device-ownership-thumbnail']/div/div/div/p")
  public List<WebElement> ThumbnailDeviceTypeList;

  /** PageObject for device report */
  @FindBy(xpath = "//div[@id = 'devicePanel_thumb']/div/div/a")
  public WebElement UnsDeviceThumbnailReport;

  @FindBy(xpath = "//div[text() = 'Device Ownership']")
  public WebElement UnsDeviceReportTitle;

  @FindBy(
    css =
        "#devicePanelReport .usDevices.details .highcharts-container > svg > g.highcharts-series-group > g.highcharts-series > rect"
  )
  public List<WebElement> UnsDeviceBarChart;

  @FindBy(xpath = "//div[@class='highcharts-label highcharts-tooltip highcharts-color-undefined']/span/span[@class='tooltip-header']")
  public WebElement UnsDeviceReportTooltipHeader;

  @FindBy(xpath = "//span[@class='series single series_0']")
  public WebElement UnsDeviceReportTooltipIndex;

  /** PageObject for Site Domain report */
  @FindBy(xpath = "//div[@id='siteDomainPanel_thumb']/div/div/a")
  public WebElement UnsSiteDomainThumbnailReport;

  @FindBy(xpath = "//div[text() = 'Top Site Domains']")
  public WebElement UNSSiteDomainReportTitle;

  @FindBy(css = "div.detail-view td.site-domain-domain-name")
  public List<WebElement> SiteDomainLinks;

  @FindBy(xpath = "//div[@class='volume-filter']")
  public WebElement UNSSiteDomainReportVolumeFilter;

  @FindBy(xpath = "//input[@type='checkbox']")
  public WebElement UNSSiteDomainReportVolumeCheckBoxes;

  @FindBy(xpath = "//button[@id='sitedomain-categoryAll Categories']")
  public List<WebElement> UNSSiteDomainReportAllCategoriesButton;

  @FindBy(xpath = "//div[@class='sd-parsed-category']")
  public List<WebElement> UNSSiteDomainReportCategoryName;

  @FindBy(xpath = "//label[@class='bucket-checkbox-inline'][1]")
  public List<WebElement> UNSSiteDomainReportVolumeHighCheckbox;

  @FindBy(xpath = "//label[@class='bucket-checkbox-inline'][2]")
  public List<WebElement> UNSSiteDomainReportVolumeMediumCheckbox;

  @FindBy(xpath = "//label[@class='bucket-checkbox-inline'][3]")
  public List<WebElement> UNSSiteDomainReportVolumeLowCheckbox;

  @FindBy(xpath = "//span[@class='bucket-label bucket-high']")
  public WebElement UNSSiteDomainReportVolumeFilterHigh;

  @FindBy(xpath = "//span[@class='bucket-label bucket-medium']")
  public WebElement UNSSiteDomainReportVolumeFilterMedium;

  @FindBy(xpath = "//span[@class='bucket-label bucket-low']")
  public WebElement UNSSiteDomainReportVolumeFilterLow;

  @FindBy(xpath = "//span[@class='volume-level high']")
  public List<WebElement> UNSSiteDomainReportVolumeValueAsHigh;

  @FindBy(xpath = "//span[@class='volume-level medium']")
  public List<WebElement> UNSSiteDomainReportVolumeValueAsMedium;

  @FindBy(xpath = "//span[@class='volume-level low']")
  public List<WebElement> UNSSiteDomainReportVolumeValueAsLow;

  @FindBy(xpath = "//div[@class='dropdown open btn-group']/ul/li")
  public List<WebElement> SiteDomainAllCategoriesList;

  @FindBy(xpath = "//div[@class='sd-parsed-category']")
  public List<WebElement> SiteDomainsWithCategorySelected;

  @FindBy(xpath = "//tr[@class='standard-row']/td[1]")
  public List<WebElement> SiteDomainsNames;

  /** PageObject for temporal report */
  @FindBy(xpath = "//div[@id = 'temporalPanel_thumb']/div/div/a")
  public WebElement UnsTemporalThumbnailReport;

  @FindBy(xpath = "//div[text() = 'Temporal Trends']")
  public WebElement UnsTemporalReportTitle;

  @FindBy(xpath = "//span[contains(text() , 'Time of Day')]")
  public WebElement todButton;

  @FindBy(xpath = "//span[contains(text() , 'Day of week')]")
  public WebElement dowButton;

  @FindBy(xpath = "//div[@class = 'geo-performance-thumbnail thumbnail']/div/div/p")
  public List<WebElement> UNSGeo_ThumbnailCity;

  @FindBy(xpath = "//div[@id = 'geoPanel_thumb']/div/div/a")
  public WebElement UNSGeo_Thumbnail;

  @FindBy(xpath = "//div[text() = 'Geo Performance']")
  public WebElement UNSGeoReportTitle;

  @FindBy(xpath = "//div[@class = 'query-text']")
  public WebElement searchQueryName;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text >tspan"
  )
  public List<WebElement> todXaxis;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-yaxis-labels > text >tspan"
  )
  public List<WebElement> todYaxis;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(1) "
  )
  public WebElement Mon;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(2) "
  )
  public WebElement Tue;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(3) "
  )
  public WebElement Wed;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(4) "
  )
  public WebElement Thu;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(5) "
  )
  public WebElement Fri;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(6) "
  )
  public WebElement Sat;

  @FindBy(
    css =
        ".highcharts-container > svg > g.highcharts-axis-labels.highcharts-xaxis-labels > text:nth-child(7) "
  )
  public WebElement Sun;

  @FindBy(xpath = "//div[@class = 'share-url']")
  public WebElement ShareURLBox;

  @FindBy(xpath = "//span[@class = 'caret']")
  public WebElement GeoButton;

  @FindBy(xpath = "//li[@class = 'geo-picker-item']/a")
  public List<WebElement> GeosList;

  @FindBy(xpath = "//div[@class='geo-picker-item-name']")
  public List<WebElement> GeosNames;

  @FindBy(
    css =
        "#export-options > div.popover-content > div.export-option.share > div:nth-child(1) > input"
  )
  public WebElement exporturlbox;
}
