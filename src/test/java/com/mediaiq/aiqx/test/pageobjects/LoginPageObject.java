package com.mediaiq.aiqx.test.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.mediaiq.aiqx.test.Base;

/** This class is used to store all the webelement xpath position in Login page */
public class LoginPageObject extends Base {

  public LoginPageObject(WebDriver driver) {
    super(driver);
  }

  @FindBy(id = "username")
  public WebElement userName;

  @FindBy(id = "password")
  public WebElement password;

  @FindBy(xpath = "//button[text() = 'SIGN IN']")
  public WebElement signInButton;

  @FindBy(xpath = "//a[text() = 'SIGN UP']")
  public WebElement signup;

  @FindBy(xpath = "//a[text() = 'MiQ User Login']")
  public WebElement MIQUserLogin;

  @FindBy(xpath = "//div[@class = 'user-info']")
  public WebElement UserLogoutOption;

  @FindBy(xpath = "//a[text() = 'Logout']")
  public WebElement LogoutButton;

  @FindBy(xpath = "//input[@type = 'email']")
  public WebElement googleemail;

  @FindBy(xpath = "//div[@id = 'password']/div[1]/div/div[1]/input")
  public WebElement googlepasssword;

  @FindBy(xpath = "//span[text() = 'Next'] ")
  public WebElement NextButton;

  @FindBy(name = "name")
  public WebElement aiqxaccessname;

  @FindBy(name = "email")
  public WebElement aiqxaccessemail;

  @FindBy(name = "organization")
  public WebElement aiqxaccessorganization;

  @FindBy(name = "jobTitle")
  public WebElement aiqxaccessjobtitle;

  @FindBy(name = "phoneNumber")
  public WebElement aiqxaccessphonenumber;

  @FindBy(xpath = "//input[@value = 'email']")
  public WebElement aiqxaccessemailoption;

  @FindBy(xpath = "//button[text() = 'Cancel']")
  public WebElement aiqxaccesscanceloption;

  @FindBy(xpath = "//div[@class = 'login-header']/h3")
  public WebElement aiqxacknowledgemessage;

  @FindBy(xpath = "//div[@class = 'logo']")
  public WebElement AIQXLogo;

  @FindBy(xpath = "//div[text() = 'Invalid username or password!']")
  public WebElement loginerrormesssage;

  @FindBy(xpath = "//div[text() = 'DISCOVER']")
  public WebElement AIQXLandingPage_DiscoverTitle;

  @FindBy(xpath = "//div[@class = 'menu-icon pull-left']")
  public WebElement AIQMenuBar;

  @FindBy(xpath = "//span[text() = 'DISCOVER']")
  public WebElement AIQXDiscoverOption;

  @FindBy(xpath = "//span[text() = 'PLAN']")
  public WebElement AIQXPlanOption;

  @FindBy(xpath = "//span[text() = 'ACTIVATE']")
  public WebElement AIQXActivateOption;

  @FindBy(xpath = "//span[text() = 'CONNECT']")
  public WebElement AIQXConnectOption;

  @FindBy(xpath = "//div[@id = 'root']/div/div[2]/button")
  public WebElement AIQXDriftButtton;

  @FindBy(xpath = "//span[contains(text() , 'Search for a company name and')]")
  public WebElement AIQXDiscoverText;

  @FindBy(
    css =
        "#side-bar-content-wrapper > div > div.content-sidebar-class > div > div > div.close-icon.pull-left"
  )
  public WebElement AIQXMenuBarCloseButton;

  @FindBy(xpath = "//div[@class = 'user-name']")
  public WebElement LoggedUserName;

  @FindBy(xpath = "//input[@type = 'text']")
  public WebElement SearchBox;

  @FindBy(xpath = "//div[text() = 'DISCOVER']")
  public WebElement DiscoverTitle;

  @FindBy(xpath = "//button[@type = 'submit']")
  public WebElement SubmitButton;

  @FindBy(xpath = "//div[@class = 'double-bounce1']")
  public WebElement LoadingIcon;

  @FindBy(xpath = "//button[@class = 'joyride-beacon']/span[2]")
  public WebElement AppWalkthroughButton;

  @FindBy(xpath = "//button[text() = 'Last']")
  public WebElement LastButton;

  @FindBy(xpath = "//div[@class = 'query-text']")
  public WebElement SearchQueryText;

  @FindBy(xpath = "//div[@class = 'search-term pull-left']/div[3]/span")
  public WebElement entitytype;
}
