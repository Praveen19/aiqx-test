Feature: Validating the keyword micro service

  Background:
    * def keywordpostbody = read('classpath:testData/KeywordMicroServices.json')

  Scenario Outline: Fetch the detail of the keyword in unique geo
    Given url 'http://api.aiq.io/keyword/search'
    And header Authorization = 'Basic ZGV2dXNlcjpzM2dtZW50bTN0YWRhdCE='
    * set keywordpostbody.id = <keywordid>
    * set keywordpostbody.geo = <usergeo>
    And request keywordpostbody
    When method post
    Then status 200


    Examples:
      | keywordid | usergeo |
      | 120029    | 'GB'    |

  Scenario Outline: Fetch the detail of the keyword in multiple geo
    Given url 'http://api.aiq.io/keyword/search'
    And header Authorization = 'Basic ZGV2dXNlcjpzM2dtZW50bTN0YWRhdCE='
    * set keywordpostbody.id = <keywordid>
    And request keywordpostbody
    When method post
    Then status 200

    Examples:
      | keywordid |
      | 79105     |

  Scenario Outline: Validate the request with invalid keyword id
    Given url 'http://api.aiq.io/keyword/search'
    And header Authorization = 'Basic ZGV2dXNlcjpzM2dtZW50bTN0YWRhdCE='
    * set keywordpostbody.id = <keywordid>
    And request keywordpostbody
    When method post
    * match response.statusCode == 400
    * match response.message == 'Bad request.'

    Examples:
      | keywordid |
      | '765fhf76'    |

  Scenario Outline: Validate the request with blank keyword id
    Given url 'http://api.aiq.io/keyword/search'
    And header Authorization = 'Basic ZGV2dXNlcjpzM2dtZW50bTN0YWRhdCE='
    * set keywordpostbody.id = <keywordid>
    And request keywordpostbody
    When method post
    * match response.statusCode == 400
    * match response.message == 'Bad request.'

    Examples:
      | keywordid |
      | ''    |

  Scenario Outline: Validate the request with empty geo
    Given url 'http://api.aiq.io/keyword/search'
    And header Authorization = 'Basic ZGV2dXNlcjpzM2dtZW50bTN0YWRhdCE='
    * set keywordpostbody.id = <keywordid>
    * set keywordpostbody.geo = <usergeo>
    And request keywordpostbody
    When method post
    * match response.statusCode == 200
    * match response.message == 'OK'

    Examples:
      | keywordid | usergeo |
      | 33130     | ''      |

  Scenario Outline: Validate the request with invalid geo
    Given url 'http://api.aiq.io/keyword/search'
    And header Authorization = 'Basic ZGV2dXNlcjpzM2dtZW50bTN0YWRhdCE='
    * set keywordpostbody.id = <keywordid>
    * set keywordpostbody.geo = <usergeo>
    And request keywordpostbody
    When method post
    * match response.statusCode == 200
    * match response.message == 'OK'
    * match response.keywordMetadataList == []

    Examples:
      | keywordid | usergeo  |
      | 33130     | '$YU&%&' |

