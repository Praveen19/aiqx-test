Feature: Validating the Save Custom Segment Functionality with Karate

  Background:
    * def postbody = read('classpath:testData/SaveCustomSegment_PostRequest.json')
    * url baseUrl


  Scenario Outline: Saving a Custom segment created by Admin User
    Given path '/customsegment/save'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MjU4NDMxNDQsInVzZXJfbmFtZSI6InByYXZlZW5AbWVkaWFpcWRpZ2l0YWwuY29tIiwic2NvcGUiOltdLCJhdXRob3JpdGllcyI6WyJCSV9TVVBFUl9VU0VSIiwiQUlRWF9DTElFTlQiLCJERVYiLCJCSV9TVEFHSU5HX1VTRVIiLCJBSVFYX0NMSUVOVF9BRE1JTiIsIkJJX0FEVkFOQ0VEX1VTRVIiXSwianRpIjoiZDhlN2UwZTItN2I1ZC00NDA5LThkNTUtMzNkMjc4ZjBhMDUzIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.d08oYBeiLhvNOMHyIA699_yVQLvdjgW9l36DoLGQjIgDio8XGyS93UXpSdGWbJUCCJRYIEKhJHSFrsJVDKWbsncXgHNzvlFwUOTRYPs5CWqP7tjrXGrnDtZHSNbFzrAO6S2VZCl3JEp6WcvRMFXmVlkExn-041XZaO_MGjuPiv1L0l9h40ri4dcrnFxTjBovqtjkNakg1qkKLIFwJfzBTDoJoJybImFMtElWvlgda0qgmXw9c0_0rNxqZnzroGASNLAhlHz-dujZjc776DHg9TQedaXTcuw_XM0kJpyeu__rEmV_F7f9QGsqmiC7WNn3QQAO0D6i4j-tluWbaO_gKg'
    * set postbody.segmentName = <name>
    And request postbody
    When method post
    Then status 200

    Examples:
      | name           |
      | 'hellobhai'    |
      | 'hellobrother' |

  Scenario Outline: Create a custom segment when existing segment present with same details
    Given path '/customsegment/save'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    * set postbody.segmentName = <name>
    And request postbody
    When method post
    Then status 500
    * match response.message == 'Failed. Ensure there are no missing or duplicate parameters.'

    Examples:
      | name           |
      | 'hellobhai'    |
      | 'hellobrother' |

  Scenario Outline: Editing the Custom Segment by Admin User
    Given path '/customsegment/edit'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MjU4NDMxNDQsInVzZXJfbmFtZSI6InByYXZlZW5AbWVkaWFpcWRpZ2l0YWwuY29tIiwic2NvcGUiOltdLCJhdXRob3JpdGllcyI6WyJCSV9TVVBFUl9VU0VSIiwiQUlRWF9DTElFTlQiLCJERVYiLCJCSV9TVEFHSU5HX1VTRVIiLCJBSVFYX0NMSUVOVF9BRE1JTiIsIkJJX0FEVkFOQ0VEX1VTRVIiXSwianRpIjoiZDhlN2UwZTItN2I1ZC00NDA5LThkNTUtMzNkMjc4ZjBhMDUzIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.d08oYBeiLhvNOMHyIA699_yVQLvdjgW9l36DoLGQjIgDio8XGyS93UXpSdGWbJUCCJRYIEKhJHSFrsJVDKWbsncXgHNzvlFwUOTRYPs5CWqP7tjrXGrnDtZHSNbFzrAO6S2VZCl3JEp6WcvRMFXmVlkExn-041XZaO_MGjuPiv1L0l9h40ri4dcrnFxTjBovqtjkNakg1qkKLIFwJfzBTDoJoJybImFMtElWvlgda0qgmXw9c0_0rNxqZnzroGASNLAhlHz-dujZjc776DHg9TQedaXTcuw_XM0kJpyeu__rEmV_F7f9QGsqmiC7WNn3QQAO0D6i4j-tluWbaO_gKg'
    * set postbody.segmentName = 'hellobhai'
    * set postbody.queryProperties[1].query = <query1>
    * set postbody.queryProperties[1].queryMetaData.keywordId = <id>
    And request postbody
    When method post
    Then status 200

    Examples:
      | query1   | id    |
      | 'LENOVO' | 79105 |


  Scenario: Rename the Custom Segment
    Given path '/customsegment/rename'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    * set postbody.segmentName = 'hellobrother'
    * set postbody.newSegmentName = 'AutomatedSegment1605'
    And request postbody
    When method post
    Then status 200

  Scenario: Rename the Custom Segment with the same name
    Given path '/customsegment/rename'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    * set postbody.segmentName = 'hellobrother'
    * set postbody.newSegmentName = 'AutomatedSegment1605'
    And request postbody
    When method post
    Then status 500
    * match response.message == 'Failed. Ensure there are no missing or duplicate parameters.'

  Scenario: Rename the Custom Segment with the existing custom segment name
    Given path '/customsegment/rename'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    * set postbody.segmentName = 'hellobhai'
    * set postbody.newSegmentName = 'AutomatedSegment1605'
    And request postbody
    When method post
    Then status 500
    * match response.message == 'Failed. Ensure there are no missing or duplicate parameters.'

  Scenario Outline: Deleting the Custom Segment created by Admin User
    Given path '/customsegment/delete'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    * set postbody.segmentName = <Name>
    And request postbody
    When method post
    Then status 200

    Examples:
      | Name                   |
      | 'AutomatedSegment1605' |
      | 'hellobhai'            |

  Scenario Outline: Delete the already deleted custom segment
    Given path '/customsegment/delete'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    * set postbody.segmentName = <Name>
    And request postbody
    When method post
    Then status 500
    * match response.message == 'Failed. Ensure there are no missing or duplicate parameters.'

    Examples:
      | Name                   |
      | 'AutomatedSegment1605' |
      | 'hellobhai'            |



