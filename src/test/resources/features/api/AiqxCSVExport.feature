@CSVExport
Feature: Validating the CSV Export Functionlaity with Karate

  Background:
    * def postbody = read('classpath:testData/export-csv-request.json')
    * configure printEnabled = false
    * url baseUrl

  Scenario: Hit the service for keyword report for 'fashion' keyword
    Given path '/exportCSV'
    And header Authorization = 'eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUyMjExNjksInVzZXJfbmFtZSI6ImRldnVzZXIiLCJzY29wZSI6W10sImF1dGhvcml0aWVzIjpbIkFJUVhfQ0xJRU5UIiwiQUlRWF9DTElFTlRfQURNSU4iLCJBSVFYX1JGUF9WSUVXRVIiXSwianRpIjoiMjdkNzk4NDItNTViMC00N2Y4LWIwZTUtYTllZWQ4ZTAwZDMwIiwiY2xpZW50X2lkIjoiYWlxeC11aSJ9.ARNhv31X4u4vBNRtDzaWSP7j7Ltvnru7YdiN8nh65OU_QDXs7lBudGlrmk8jKrY_RFvu9Z5vL9X1n90eQzFeqkvdzMcT3Y1hIutiExSv9ODJXjUoaOK4emH4_n4aQub94LyUCda7PxbvpNLhN9T9W5VaWxTLvMNu31RY-KdJ43SEJxWVrQcUbzFIoZO99-RkSjUgJTaH7l9DwfMdXlmFJ5Zp5ldaABHK7qXTXPTViOvkhPBpJEhSkWAJeY8PQcULdY1_mdFeffuSMSY0PQlNAlHSr3mIxXJBK3KDXb6OcFphnTsJTdYjA06KYLdpX5Gezwi05XT8Tf2HW34vsZhqmg'
    And request postbody
    When method post
    Then status 200
    Then assert response !== null



