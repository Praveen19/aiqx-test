#Author Praveen Kumar
Feature: Validating all the test cases for Keyword Report

  @Start
  Scenario Outline: Log into AIQx App
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button

    Examples:
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |


  Scenario Outline: Validation on Keyword Report thumbnail UI for different geo region when user search for any Owned Audience
    Given I triger the below test step from inititate function with the given <EntityName> and <EntityType>
    And I enter the valid entity name as <EntityName> in search box of app landing screen
    And I verify the entity type as <EntityType> in category tab
    And I click on the respective categories tab of matching <EntityType> and click on the searched entity  from autocomplete screen
    And I validate that keyword Thumbnail report should appear in the report loading screen

    Examples:
      | EntityName | EntityType     |
      | 10048207   | Owned Audience |
      | 3035428    | Audience       |
      | msn.com    | Site Domain    |
      | LADY GAGA  | Keyword        |
      | Age - 65+  | Demographic    |
      | MOBILE     | Device Type    |
      | IMDB       | Brand Affinity |

  Scenario Outline: Change the geo from geo picker
    Given I click and set the <geo> from geo picker

    Examples:
      | geo            |
      | United Kingdom |


  Scenario Outline: Keyword Detail report validation
    Given I enter the valid entity name as <Entityname> of its entity type as <EntityType> in search box of landing screen
    And I click on the keyword report thumnail
    Then I validate the keyword report title

    Examples:
      | Entityname | EntityType |
      | lenovo     | Keyword    |

  Scenario Outline: Keyword Detail Report All Categories and View button tabs Validation
    Given I should be on the Keyword treemap detail report screen for the searched <Entityname> of its type as <EntityType>
    And All Categories button should be clickable
    And All categories should not contains Blacklist as category
    And View button tabs should be clickable
    And I click on tabular view button
    Then Tabular Chart should display

    Examples:
      | Entityname | EntityType |
      | 49190      | Keyword    |

#  Scenario Outline: Validation on Keyword Performance Metrix
#    Given I should be on the Keyword tabular detail report screen for the searched <Entityname> of its type as <EntityType>
#    And Performance index value on the keyword tabular detail report screen should be between minus one to plus one
#
#    Examples:
#      | Entityname | EntityType |
#      |      79897 | Keyword    |

  Scenario Outline: User can sort the keyword detail report based on any category
    Given I should be on the Keyword tabular detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on any category from the tabular chart
    And Check all the keywords sort with same clicked category
    And All categories button shoud display the same clicked category name
    And I click on the treemap button from the keyword tabular chart screen
    And I click on the All ategories button
    And I select All categories filter

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  Scenario Outline: Download Keyword Report
    Given I should be on the Keyword treemap detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I click on download as Image button
    Then Report gets downloaded of its type as keyword

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  Scenario Outline: Keyword Report share Link Functionality
    Given I should be on the Keyword treemap detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I copied the share link
    And I browse the copied share link in any browser tab
    Then Keyword Report should display for the share link

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  Scenario Outline: Like/Dislike button
    Given I should be on the Keyword treemap detail report screen for the searched <Entityname> of its type as <EntityType>
    And Like/Dislike button should be clickable

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  @End
  Scenario Outline: Duplicate keywords should not be display in Autocomplete
    Given I enter the valid keyword as <Entityname> of its entity type as <EntityType> in search box of landing screen
    Then Duplicate keywords should not display in the autocomplete for the searched entityname as <Entityname> in its respective tab of its type as  <EntityType>

    Examples:
      | Entityname | EntityType |
      | lifestyle  | Keyword    |
