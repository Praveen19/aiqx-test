#Author: Praveen
Feature: Validating all the test cases for Device Ownership Report

  @Start
  Scenario Outline: Log into AIQx App
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button

    Examples:
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

  Scenario Outline: Validation on Device Ownership Report thumbnail UI for different geo region when user search for any entity
    Given I triger the below test step from inititate function with the given <EntityName> and <EntityType>
    And I enter the valid entity name as <EntityName> in search box of app landing screen
    And I verify the entity type as <EntityType> in category tab
    And I click on the respective categories tab of matching <EntityType> and click on the searched entity  from autocomplete screen
    And I validate that Device Ownership Thumbnail report should appear in the report loading screen

    Examples:
      | EntityName  | EntityType     |
      | 8622026     | Owned Audience |
      | 10895871    | Audience       |
      | nytimes.com | Site Domain    |
      | 45533       | Keyword        |
      | Age - 45-54 | Demographic    |
      | desktop     | Device Type    |
      | Automax     | Brand Affinity |

  Scenario Outline: Change the geo from geo picker
    Given I click and set the <geo> from geo picker

    Examples:
      | geo            |
      | United Kingdom |

  Scenario Outline: Device Ownership Detail report validation
    Given I enter the valid entity name as <Entityname> of its entity type as <EntityType> in search box of landing screen
    And I click on the device ownership report thumbnail
    Then I validate the device Ownership report title

    Examples:
      | Entityname    | EntityType  |
      | NEWYORKER.COM | Site Domain |

  Scenario Outline: Device Index value validation
    Given I enter the valid entity name as <Entityname> of its entity type as <EntityType> in search box of landing screen
    And I check the name of the device type from the device thumbnail report
    And I click on the device ownership report thumbnail
    And I validate that device type showing in thumbnail rreport will have the positive index in the detail screen

    Examples:
      | Entityname                  | EntityType     |
      | ACCOR - ADV PIXEL - PAYMENT | Owned Audience |

  Scenario Outline: Like/Dislike button
    Given I should be on the device detail report screen for the searched <Entityname> of its type as <EntityType>
    And Like/Dislike button should be clickable

    Examples:
      | Entityname | EntityType  |
      | espn.com   | Site Domain |

  Scenario Outline: Download Geo Report
    Given I should be on the device detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I click on download as Image button
    Then Report gets downloaded of its type as device

    Examples:
      | Entityname    | EntityType  |
      | GENDER - MALE | Demographic |

  @End
  Scenario Outline: Device Report share Link Functionality
    Given I should be on the device detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I copied the share link of device ownership report
    And I browsed the copied share link of device report in any browser tab
    Then Device Report should display for the share link

    Examples:
      | Entityname  | EntityType  |
      | Age - 18-24 | Demographic |
