#Author: Praveen
Feature: Validating all the test cases for Temporal Trends Report

  @Start
  Scenario Outline: Log into AIQx App
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button

    Examples:
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

  Scenario Outline: Validation on Temporal Report thumbnail UI for different geo region when user search for any entity
    Given I triger the below test step from inititate function with the given <EntityName> and <EntityType>
    And I enter the valid entity name as <EntityName> in search box of app landing screen
    And I verify the entity type as <EntityType> in category tab
    And I click on the respective categories tab of matching <EntityType> and click on the searched entity  from autocomplete screen
    And I validate that Temporal Thumbnail report should appear in the report loading screen

    Examples:
      | EntityName  | EntityType     |
      | 8632053     | Owned Audience |
      | 186158      | Audience       |
      | BBC.COM     | Site Domain    |
      | AUDI A6     | Keyword        |
      | Age - 25-34 | Demographic    |
      | Desktop     | Device Type    |

  Scenario Outline: Change the geo from geo picker
    Given I click and set the <geo> from geo picker

    Examples:
      | geo            |
      | United Kingdom |

  Scenario Outline: Temporal Detail report validation
    Given I enter the valid entity name as <Entityname> of its entity type as <EntityType> in search box of landing screen
    And I click on the temporal report thumbnail
    Then I validate the temporal report title

    Examples:
      | Entityname | EntityType |
      | MOTOROLA   | Keyword    |

  Scenario Outline: Temporal Detail Report TOD and DOW button validation
    Given I should be on the Temporal trends detail report screen for the searched <Entityname> of its type as <EntityType>
    And TOD button should be clickable
    And DOW button should be clicakble

    Examples:
      | Entityname  | EntityType  |
      | weather.com | Site Domain |

  Scenario Outline: Time of day report should have no duplicate hours and index value lies between minus one to plus one
    Given I should be on the Temporal trends detail report screen for the searched <Entityname> of its type as <EntityType>
    And Hours axis should have value between zero to twentythree
    And Hour axis should have no duplicate hour value
    And I click on discover breadcrumb tab to go discover page

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

    ## Need to check this scenario
#  Scenario Outline: Day of Week report validation
#    Given I should be on the Temporal trends detail report screen for the searched <Entityname> of its type as <EntityType>
#    And I click on Day of Week button
#    And Day axis should be in sorted order
#    And I click on discover breadcrumb tab to go discover page
#
#    Examples:
#      | Entityname                  | EntityType     |
#      | ACCOR - ADV PIXEL - PAYMENT | Owned Audience |

  Scenario Outline: Like/Dislike button
    Given I should be on the Temporal trends detail report screen for the searched <Entityname> of its type as <EntityType>
    And Like/Dislike button should be clickable

    Examples:
      | Entityname | EntityType  |
      | espn.com   | Site Domain |

  Scenario Outline: Download Temporal Report
    Given I should be on the Temporal trends detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I click on download as Image button
    Then Report gets downloaded of its type as temporal

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  @End
  Scenario Outline: Temporal Report share Link Functionality
    Given I should be on the Temporal trends detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I copied the share link of temporal trend report
    And I browsed the copied share link of temporal report in any browser tab
    Then Temporal Report should display for the share link

    Examples:
      | Entityname  | EntityType  |
      | Age - 45-54 | Demographic |
