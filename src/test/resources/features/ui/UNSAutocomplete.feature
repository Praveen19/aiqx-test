#Author: Praveen Kumar
Feature: UNS Autocomplete functionality

  @Start
  Scenario Outline: Login into App and do the screenlevel validation
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button
    And I verify that search box is auto focus
    And I check the menu icon bar is clickable
    And I verify the menu list items
    And I verify the Discover text should present in the landing screen

    Examples:
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

  Scenario Outline: Autosuggest entities with substring search
    Given I enter the substring as <Substring> in search box of the laanding screen
    And I validate that the autosuggest results should contains the searched substring as <Substring>
    And I validate that at max only twenty four box should show in the autosuggest

    Examples:
      | Substring |
      | BM        |
      | LEN       |

  Scenario Outline: Autosuggest entities with exact search
    Given I enter the exact search entity as <ExactString> in search box of report landing screen
    And I validate that the autosuggest results should contains the exact search string as <ExactString>

    Examples:
      | ExactString    |
      | WEIGHT WATCHER |
      | LENOVO         |

  Scenario Outline: Autosuggest show unique result when search for any unique id
    Given I enter the any valid entity id as <ValidID> in search box of report landing screen
    And I validate that autosuggest should only display the searched result name in auto suggestion

    Examples:
      | ValidID |
      | 117422  |


  @End
  Scenario Outline: Autosuggest Categories should be clickable and should not have null result
    Given I enter the any valid string as <ValidString> in search box of landing screen
    And I validate the number of count of results in autosuggest screen should match with the count of results in landing page
    And I validate the counts of categories showing in the landing screen should match with the number of categories tabs
    And I validate that all the categories tabs are clickable
    And I validate that results in all the categories tabs are equal to the count of results in respective cetagories tab

    Examples:
      | ValidString |
      | BMW         |
