#Author: Praveen
Feature: Validating all the test cases for Top Site Domain Report

  @Start
  Scenario Outline: Log into AIQx App
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button

    Examples:
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

  Scenario Outline: Validation on Site Domain Report thumbnail UI for different geo region when user search for any entity
    Given I triger the below test step from inititate function with the given <EntityName> and <EntityType>
    And I enter the valid entity name as <EntityName> in search box of app landing screen
    And I verify the entity type as <EntityType> in category tab
    And I click on the respective categories tab of matching <EntityType> and click on the searched entity  from autocomplete screen
    And I validate that Site Domain Thumbnail report should appear in the report loading screen

    Examples:
      | EntityName       | EntityType     |
      | 10048230         | Owned Audience |
      | 940597           | Audience       |
      | BBC.COM          | Site Domain    |
      | CRYPTOCURRENCIES | Keyword        |
      | Age - 25-34      | Demographic    |
      | DESKTOP          | Device Type    |

  Scenario Outline: Change the geo from geo picker
    Given I click and set the <geo> from geo picker

    Examples:
      | geo            |
      | United Kingdom |

  Scenario Outline: Site Domain Detail report validation
    Given I enter the valid entity name as <Entityname> of its entity type as <EntityType> in search box of landing screen
    And I click on the site domain report thumbnail
    Then I validate the site domain report title

    Examples:
      | Entityname | EntityType |
      | MOTOROLA   | Keyword    |

  Scenario Outline: Duplicate Site Domain
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And No duplicate domains name should present

    Examples:
      | Entityname  | EntityType  |
      | weather.com | Site Domain |

  Scenario Outline: Site Domain Category Name empty validation
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And Site Domain category name should not be empty

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  Scenario Outline: Like/Dislike button
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And Like/Dislike button should be clickable

    Examples:
      | Entityname    | EntityType  |
      | Gender - Male | Demographic |

  Scenario Outline: Screen level scenario
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And All the filter checkbox option should display in detail screen
    And All category button should be enable

    Examples:
      | Entityname | EntityType |
      | LADY GAGA  | Keyword    |

  Scenario Outline: Filter domain with bucket as low
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on Low bucket filter on detail screen
    And All domains should get filter with volume as low
    And No duplicate domains name should present

    Examples:
      | Entityname      | EntityType  |
      | Gender - Female | Demographic |

  Scenario Outline: Filter domain with bucket as medium
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on Medium bucket filter on detail screen
    And All domain should get filter with volume as medium
    And No duplicate domains name should present

    Examples:
      | Entityname      | EntityType  |
      | Gender - Female | Demographic |

  Scenario Outline: Filter domain with bucket as high
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on High bucket filter with volume as high
    And All domain should filter with volume as high
    And No duplicate domains name should present

    Examples:
      | Entityname      | EntityType  |
      | Gender - Female | Demographic |

  Scenario Outline: Filter domain based on categories inside All category button
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on all categories button
    And I select any category from the all categories drop down box
    Then All the domains should get filter with the selected category

    Examples:
      | Entityname  | EntityType  |
      | Age - 35-44 | Demographic |

  Scenario Outline: Download Site Domain Report
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I click on download as Image button
    Then Report gets downloaded of its type as siteDomain

    Examples:
      | Entityname | EntityType |
      | 107838     | Keyword    |

  Scenario Outline: Domain clickable link
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    Then All the domains should be clickable
    And I click on any site domain link
    And Link should be open in new tab
    And I close the newly opened tab
    And I click on discover breadcrum tab

    Examples:
      | Entityname  | EntityType  |
      | Age - 45-54 | Demographic |

  @End
  Scenario Outline: Site Domain Report share Link Functionality
    Given I should be on the Site Domain detail report screen for the searched <Entityname> of its type as <EntityType>
    And I click on the Export Report button
    And I copied the share link of Site Domain report
    And I browsed the copied share link of site domain report in any browser tab
    Then Site Domain Report should display for the share link

    Examples:
      | Entityname  | EntityType  |
      | Age - 45-54 | Demographic |
