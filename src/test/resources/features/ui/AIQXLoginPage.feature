@Login
Feature: AIQX Login flow

  @Start
  Scenario Outline: Validation on Login Page
    Given I navigate to the aiqx URL
    And I enter invalid <username> and <password> in the login page and click on Sign In button
    Then I check the validation message in the logiin page

    Examples:
      | username                   | password |
      | praveen@mediaiqdigital.com | abcdef   |

  @End
  Scenario Outline: DISCOVER Title should appear in the UI upon valid user login.
    Given I navigate to the aiqx URL
    And I enter the Username as <username> and  Password as <password> and click on Sign in button
    Then I verify the aiqx Landing Discover title and logged username as <username>
    Then Click on Logout

    Examples:
      | username                   | password    |
      | praveen@mediaiqdigital.com | aiqxCIA2016 |

